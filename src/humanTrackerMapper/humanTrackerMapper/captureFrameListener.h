#ifndef CAPTURE_FRAME_LISTENER
#define CAPTURE_FRAME_LISTENER

// OpenNI Includes
#include <OpenNI.h>

#include <chrono>
#include <ctime>  

using namespace openni;

/**
 * Class is used for handle new frames of the camera VideoStream.
 */
class CaptureFrameListener: public VideoStream::NewFrameListener
{
public:
	/**
	 * Constructor.
	 */
	CaptureFrameListener() {};
	/**
	 * Destructor.
	 */
	~CaptureFrameListener() {};

	/**
	 * Is called if new frame of VideoStream is available.
	 * @param stream VideoStream
	 */
	void onNewFrame(VideoStream & stream) override;

	/**
	 * Returns the last received frame.
	 * @return last frame
	 */
	VideoFrameRef getFrame();
	/**
	 * Returns if last frame is newer than MAX_WAIT_TIME.
	 * @param last frame time < MAX_WAIT_TIME
	 */
	bool isFrameCurrent();
	/**
	 * Returns if received a frame yet.
	 * @return received frame yes/no
	 */
	bool receivedFrame();

private:
	/** Time in which a frame is marked as new. */
	const double MAX_WAIT_TIME = 2;
	/** Newest frame. */
	VideoFrameRef frame;
	/** Time of newest frame. */
	std::chrono::time_point<std::chrono::system_clock> timeLastFrame;
	/** Saves is received a frame yet. */
	bool frameReceived = false;
};


#endif // !CAPTURE_FRAME_LISTENER