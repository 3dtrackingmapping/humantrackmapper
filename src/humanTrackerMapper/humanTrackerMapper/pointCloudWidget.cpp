#include "pointCloudWidget.h"

/* Public */
PointCloudWidget::PointCloudWidget(QWidget *parent) :
	QOpenGLWidget(parent)
{
	mBgColor = QColor::fromRgb(0, 0, 0);
}

/* Slots */

/* Protected */
void PointCloudWidget::initializeGL() {

	makeCurrent();
	/* Init OpenGL Functions */
	initializeOpenGLFunctions();

	/* OpenGL Operations*/
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glEnable(GL_ALPHA_TEST);
	glEnable(GL_CULL_FACE);

	/* Background Color */
	float r = ((float)mBgColor.darker().red()) / 255.0f;
	float g = ((float)mBgColor.darker().green()) / 255.0f;
	float b = ((float)mBgColor.darker().blue()) / 255.0f;

	glClearColor(r, g, b, 1.0f);

	/* Shader */

	/*
	 * Point Shader
	 */
	{
		pointShader = new QOpenGLShaderProgram(this);
		pointShader->addShaderFromSourceFile(QOpenGLShader::Vertex, "./Ressources/Shader/PointShader/pointShader.vert");
		pointShader->addShaderFromSourceFile(QOpenGLShader::Fragment, "./Ressources/Shader/PointShader/pointShader.frag");
		pointShader->link();
		/* Get Shader Locations */
		{
			pointShader_projection = pointShader->uniformLocation("projection");
			pointShader_view = pointShader->uniformLocation("view");
		}
	}


	/*
	 * Line Shader
	 */
	{
		lineShader = new QOpenGLShaderProgram(this);
		lineShader->addShaderFromSourceFile(QOpenGLShader::Vertex, "./Ressources/Shader/LineShader/lineShader.vert");
		lineShader->addShaderFromSourceFile(QOpenGLShader::Fragment, "./Ressources/Shader/LineShader/lineShader.frag");
		lineShader->link();
		/* Get Shader Locations */
		{
			lineShader_projection = lineShader->uniformLocation("projection");
			lineShader_view = lineShader->uniformLocation("view");
			lineShader_model = lineShader->uniformLocation("model");
		}
	}

	/*
	 * Phong Shader
	 */
	{
		phongShader = new QOpenGLShaderProgram(this);
		phongShader->addShaderFromSourceFile(QOpenGLShader::Vertex, "./Ressources/Shader/modelVertexShader.vs");
		phongShader->addShaderFromSourceFile(QOpenGLShader::Fragment, "./Ressources/Shader/modelFragmentShader.fs");
		phongShader->link();

		/* Get Shader Location */
		{
			phongShader_projection = phongShader->uniformLocation("projection");
			phongShader_view = phongShader->uniformLocation("view");
			phongShader_model = phongShader->uniformLocation("model");

			phongShader_viewPos = phongShader->uniformLocation("viewPos");
			phongShader_intensity = phongShader->uniformLocation("intensity");
			phongShader_objColor = phongShader->uniformLocation("objectColor");
		}
	}

	/* 
	 * Object 
	 */
	{
		object = new Object3D(lineShader);
		/* DEBUG set position */
		object->setPosition({ 0.0f, 0.0f, 0.0f });
		object->setScale({ 0.249f, 0.066f, 0.067f });
	}

	/*
	 * Point Cloud VAO & Buffer Binding
	 */
	{
		vector<float> pos = {};
		vector<float> color = {};

		pointShader->bind();

		/* Point Cloud */
		pc_vao = new QOpenGLVertexArrayObject(this);
		pc_vao->create();
		pc_vao->bind();
			pc_vbo_pos = new QOpenGLBuffer();
			pc_vbo_pos->create();
			pc_vbo_pos->setUsagePattern(QOpenGLBuffer::DynamicDraw);
			pc_vbo_pos->bind();
				pc_vbo_pos->allocate(pos.data(), pos.size() * sizeof(float));

				pointShader->setAttributeBuffer(0, GL_FLOAT, 0, 3);
				pointShader->enableAttributeArray(0);

			pc_vbo_color = new QOpenGLBuffer();
			pc_vbo_color->create();
			pc_vbo_color->setUsagePattern(QOpenGLBuffer::DynamicDraw);
			pc_vbo_color->bind();
				pc_vbo_color->allocate(color.data(), color.size() * sizeof(float));

				pointShader->setAttributeBuffer(5, GL_FLOAT, 0, 3);
				pointShader->enableAttributeArray(5);

			pc_size = pos.size() / 3;

			pc_vbo_pos->release();
			pc_vbo_color->release();

		pc_vao->release();
	}

	/*
	 * Vectors VAO & Buffer Binding
	 */
	{
		vector<float> pos = {};
		vector<float> color = {};

		lineShader->bind();

		/* Point Cloud */
		vec_vao = new QOpenGLVertexArrayObject(this);
		vec_vao->create();
		vec_vao->bind();
			vec_vbo_pos = new QOpenGLBuffer();
			vec_vbo_pos->create();
			vec_vbo_pos->setUsagePattern(QOpenGLBuffer::DynamicDraw);
			vec_vbo_pos->bind();
				vec_vbo_pos->allocate(pos.data(), pos.size() * sizeof(float));

				lineShader->setAttributeBuffer(0, GL_FLOAT, 0, 3);
				lineShader->enableAttributeArray(0);

			vec_vbo_color = new QOpenGLBuffer();
			vec_vbo_color->create();
			vec_vbo_color->setUsagePattern(QOpenGLBuffer::DynamicDraw);
			vec_vbo_color->bind();
				vec_vbo_color->allocate(color.data(), color.size() * sizeof(float));

				lineShader->setAttributeBuffer(5, GL_FLOAT, 0, 3);
				lineShader->enableAttributeArray(5);

			vec_size = pos.size() / 3;

			vec_vbo_pos->release();
			vec_vbo_color->release();

		vec_vao->release();
	}


	/* Camera */
	{
		cam = new Camera(QVector3D(-0.024806f, -0.004873f, 0.057535f));
		cam->setYaw(-90);
		cam->setPitch(0);
	}

	/* Light */
	light = new PointLight();

	/* Model */
	Model * head = new Model("./Ressources/objects/head/male_head_obj_centered_mask.obj");
	head->setScale({ 0.006f,0.006f,0.006f });
	//head->setRotation(QQuaternion(qDegreesToRadians(90.0f),1,0,0));
	model = head;
};

void PointCloudWidget::paintGL() {
	makeCurrent();

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	draw3DScene();
};

void PointCloudWidget::resizeGL(int width, int height) {
	makeCurrent();
	glViewport(0, 0, (GLint)width, (GLint)height);
	mWidth = width;
	mHeight = height;

	glMatrixMode(GL_PROJECTION);						//Set to Projection Mode
	glLoadIdentity();

	glOrtho(0, width, -height, 0, 0, 1);

	glMatrixMode(GL_MODELVIEW);							// Matrix Mode to Model View

	//recalculatePosition();

	//emit imageSizeChanged(mRenderWidth, mRenderHeight);

	updateScene();
};

void PointCloudWidget::updateScene() {
	if (this->isVisible()) update();
};

void PointCloudWidget::draw3DScene() {
	// configure global opengl state
	// -----------------------------
	glEnable(GL_DEPTH_TEST);		// Enable Depth Test for transparenz

	// viewMatrix
	QMatrix4x4 view;
	
	view = cam->GetViewMatrix();

	// projectionMatrix 
	QMatrix4x4 projection;
	projection.perspective(60, 1920 / 1080, 0.1f, 5000.0f);

	/*
	 * Bind Uniforms
	 */
	// Phong Shader
	phongShader->bind();				// Activate Shader
		phongShader->setUniformValue(phongShader_viewPos, cam->Position);
		phongShader->setUniformValue(phongShader_view, view);
		phongShader->setUniformValue(phongShader_projection, projection);
		//Transparency of Object
		phongShader->setUniformValue(phongShader_intensity, 1.0f);
		//Point Light
		light->Draw(phongShader);
	phongShader->release();			//deactivate Shader
	// Line Shader
	lineShader->bind();
		lineShader->setUniformValue(lineShader_view, view);
		lineShader->setUniformValue(lineShader_projection, projection);
	lineShader->release();
	// Point Shader 
	pointShader->bind();
		pointShader->setUniformValue(pointShader_view, view);
		pointShader->setUniformValue(pointShader_projection, projection);
	pointShader->release();

	/* 
	 * draw Camera 
	 */
	if(showCam){
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
		lineShader->bind();				// Activate Shader
			//Set Color of Object (TODO: NOT NECESSARY IF TEXTURES EXIST)
			lineShader->setUniformValue(phongShader_objColor, QVector3D(0.7, 0.7, 0.7));

			object->draw(lineShader);

			lineShader->release();			//Deativate Shader
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	}

	/**
	 * Draw Head
	 */
	if (showMod) {
		phongShader->bind();
		phongShader->setUniformValue(phongShader_objColor, QVector3D(1.0, 1.0, 1.0));

		model->Draw(phongShader);
		phongShader->release();			//Deativate Shader
	}

	/* 
	 * draw Points 
	 */
	if(showPC){
		pointShader->bind();
		pc_vao->bind();
			glEnable(GL_PROGRAM_POINT_SIZE);
			glDrawArrays(GL_POINTS, 0, pc_size);
		pc_vao->release();
		pointShader->release();
	}

	/*
	 * draw Lines
	 */
	if(showVecs){
		lineShader->bind();
		vec_vao->bind();
			QMatrix4x4 model;
			model.setToIdentity();
			lineShader->setUniformValue(lineShader_model, model);
		 
			glDrawArrays(GL_LINES, 0, vec_size);
		vec_vao->release();
		lineShader->release();
	}

}

void PointCloudWidget::mousePressEvent(QMouseEvent *event) {
	if (event->buttons() == Qt::LeftButton) {
		LMB_Pressed = true;
	}

	if (event->buttons() == Qt::MidButton) {
		MMB_Pressed = true;
	}

	if (event->buttons() == Qt::RightButton) {
		RMB_Pressed = true;
	}
}

void PointCloudWidget::mouseReleaseEvent(QMouseEvent *event) {

	/* Get Mouse Button Pressed */
	if (LMB_Pressed && event->buttons() != Qt::LeftButton) {
		firstMouse = true;
		LMB_Pressed = false;
	}

	if (MMB_Pressed && event->buttons() != Qt::MidButton) {
		firstMouse = true;
		MMB_Pressed = false;
	}

	if (RMB_Pressed && event->buttons() != Qt::RightButton) {
		firstMouse = true;
		RMB_Pressed = false;
	}
}

void PointCloudWidget::mouseMoveEvent(QMouseEvent * event)
{

	int xpos = event->pos().x();
	int ypos = event->pos().y();

	if (firstMouse)
	{
		lastX = xpos;
		lastY = ypos;
		firstMouse = false;
	}

	float xoffset = xpos - lastX;
	float yoffset = lastY - ypos;
	// reversed since y-coordinates range from bottom to top
	lastX = xpos;
	lastY = ypos;

	/* Check if Alt Key is pressed! */
	if (QGuiApplication::queryKeyboardModifiers().testFlag(Qt::AltModifier))
	{
		/* Rotation */
		if (LMB_Pressed) {
			cam->rotate(xoffset, yoffset);
		}

		/* Translation */
		if (MMB_Pressed) {
			if (xoffset > 0) {
				cam->translate(LEFT, xoffset*0.005);
			}
			else {
				cam->translate(RIGHT, xoffset*-0.005);
			}

			if (yoffset > 0) {
				cam->translate(DOWN, yoffset*0.005);
			}
			else {
				cam->translate(UP, yoffset*-0.005);
			}
		}
		
		/*  Zoom */
		if (RMB_Pressed) {
			if (xoffset > 0) {
				cam->translate(FORWARD, xoffset*0.005);
			}
			else {
				cam->translate(BACKWARD, xoffset*-0.005);
			}
		}
	}



}

void PointCloudWidget::drawVectors(vector<float> vecs, vector<float> colors)
{
	drawMutex.lock();

	lineShader->bind();

	vec_vbo_pos->bind();
	vec_vbo_pos->allocate(vecs.data(), vecs.size() * sizeof(float));

	vec_vbo_color->bind();
	vec_vbo_color->allocate(colors.data(), colors.size() * sizeof(float));

	vec_size = vecs.size() / 3;

	vec_vbo_pos->release();
	vec_vbo_color->release();

	drawMutex.unlock();
}

void PointCloudWidget::setObjectPosition(float x, float y, float z)
{
	model->setPosition({ x, y, z });
}
void PointCloudWidget::setObjectRotation(cv::Point3f dirVec)
{
	model->setRotation(QVector3D(dirVec.x, dirVec.y, -dirVec.z));
}

void PointCloudWidget::drawPointCloud(vector<float> pc)
{
	vector<float> color;
	for (int i = 0; i < pc.size(); i++) {
		color.push_back(1.0);
	}

	drawMutex.lock();

	pointShader->bind();

		pc_vbo_pos->bind();
		pc_vbo_pos->allocate(pc.data(), pc.size() * sizeof(float));

		pc_vbo_color->bind();
		pc_vbo_color->allocate(color.data(), color.size() * sizeof(float));

	pc_size = pc.size() / 3;

	pc_vbo_pos->release();
	pc_vbo_color->release();

	drawMutex.unlock();
};

/* Private*/