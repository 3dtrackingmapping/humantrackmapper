#ifndef POINTCLOUD_TAB_H
#define POINTCLOUD_TAB_H

// Qt 
#include <QtGui/QGuiApplication>
#include <QOpenGLWidget>
#include <QGLBuffer>
#include <QGLformat>
#include <QOpenGLFunctions>
#include <QtCore/qmath.h>
#include <QtGui/QMatrix4x4>
#include <QtGui/QOpenGLShaderProgram>
#include <QMouseEvent>

// OpenCV
#include <opencv2/opencv.hpp>
#include <opencv2/core/core.hpp>

#include <mutex>

// 3D
#include "pointLight.h"
#include "object3d.h"
#include "camera.h"
#include "model.h"

/**
 * Class implements new QWidget, which shows Camera-Image and 3D Object.
 */

class PointCloudWidget : public QOpenGLWidget, protected QOpenGLFunctions
{
	Q_OBJECT
public:
	/** Constructor */
	explicit PointCloudWidget(QWidget *parent = 0);

	void drawPointCloud(vector<float> pc);
	void drawVectors(vector<float> vecs, vector<float> colors);
	/** Change position of 3D Object */
	void setObjectPosition(float x, float y, float z);
	void setObjectRotation(cv::Point3f dirVec);


	/*
	 * Flags
	 */
	bool showMod = true;
	bool showPC = true;
	bool showVecs = true;
	bool showCam = true;

public slots:

signals:
	/** Change image size, when widget size is changed */
	void imageSizeChanged(int outW, int outH);
protected:
	/** initialize OpenGL */
	void initializeGL();							/// OpenGL initialization
	void paintGL();									/// OpenGL Rendering
	void resizeGL(int width, int height);			/// Widget Resize Event

	void updateScene();
	void draw3DScene();
	void mouseMoveEvent(QMouseEvent *event);

	void mousePressEvent(QMouseEvent *event);
	void mouseReleaseEvent(QMouseEvent *event);

private:
	/* 
	 * OpenGL Variables 
	 */
	QColor      mBgColor;							/// Background color

	Camera *cam;									/// Camera


	/* 
	 * Window 
	 */
	int mWidth;
	int mHeight;

	/* 
	 * Mouse 
	 */
	bool firstMouse = true;

	bool LMB_Pressed = false;
	bool MMB_Pressed = false;
	bool RMB_Pressed = false;

	float lastX = 0;
	float lastY = 0;

	/* 
	 * Shader 
	 */
	// Point Shader Program
	QOpenGLShaderProgram *pointShader;
		/*
		 * Shader Variables
		 */
		GLuint pointShader_projection;
		GLuint pointShader_view;

	// Line Shader Program
	QOpenGLShaderProgram *lineShader;
		/*
		 * Shader Variables
		 */
		GLuint lineShader_projection;
		GLuint lineShader_view;
		GLuint lineShader_model;

	// Phong Shader Program
	QOpenGLShaderProgram *phongShader;					/// Shader Programm
		/*
		 * Shader Variables
		 */
		GLuint phongShader_projection;
		GLuint phongShader_view;
		GLuint phongShader_model;
		GLuint phongShader_intensity;
		GLuint phongShader_viewPos;
		GLuint phongShader_objColor;



	PointLight *light;								///Light(s)

	/*
	 * 3D Volumes
	 */
	Object3D *object;								///3D Object
	Model *model;									///3D Model


	std::mutex drawMutex;

	/**
	 * Point Cloud 
	 */

	/* Buffer Objects */
	QOpenGLVertexArrayObject * pc_vao;
	QOpenGLBuffer * pc_vbo_pos;
	QOpenGLBuffer * pc_vbo_color;
	int pc_size = 0;

	/**
	 * Vectors 
	 */
	QOpenGLVertexArrayObject * vec_vao;
	QOpenGLBuffer * vec_vbo_pos;
	QOpenGLBuffer * vec_vbo_color;
	int vec_size = 0;
};

#endif // !POINTCLOUD_TAB_H
