#include "humanTrackerMapper.h"

/**
 * Constructor. Initializes Components and starts Camera-Timer.
 *
 * @param parent Parent-Object
 */
HumanTrackerMapper::HumanTrackerMapper(QWidget *parent) :
	QMainWindow(parent),
	ui(new Ui::HumanTrackerMapper),
	faceRegionDetector()
{
	// setup UI
	ui->setupUi(this);

	// initialize the program
	initialization();

	// set face detection settings
	//faceRegionDetector.disableVisualization();
	faceRegionDetector.setShowDetectedFaceRegion(true);
	faceRegionDetector.setShowLandmarks(true);
	faceRegionDetector.setShowTrackedFaceRegion(true);

	// connect camera menu
	connect(ui->menu_Camera, SIGNAL(aboutToShow()), this, SLOT(update_Devices()));

	/* Initialise Landmarks */
	facemarks = new FaceLandmarks();
}

/**
 * Initializes the Kinect and the Camera timer.
 */
void HumanTrackerMapper::initialization() {
	try {
#if USE_KINECT_EMU
		/*
			sample_01 middle front
			sample_10 left back
			sample_12 right back
		*/

		std::string fileName = "sample_02";
		// create Kinect emulator device
		kinect = new Kinect_EMU(fileName);
#else
		// create Kinect device 
		kinect = new Kinect();
#endif // USE_KINECT_EMU
		// starts timer for timer event
		timerIdentifier = startTimer(FRAMES_PER_SECOND);

		// check if timer initialization was successfull
		if (!timerIdentifier) {
			int ret = QMessageBox::warning(this, "Timer Initialization failed", "Start of Timer failed.", QMessageBox::Close | QMessageBox::Retry, QMessageBox::Retry);

			// User Input
			switch (ret) {
				// retry initialization
				case QMessageBox::Retry:
					initialization();
					break;
				// close application
				case QMessageBox::Close:
					// enable intialize action
					ui->action_Initialize->setDisabled(false);
					break;
			}
		}
		else {
			ui->statusBar->showMessage("Timer start successful.");
		}
		
		// disable intialize action
		ui->action_Initialize->setDisabled(true);
		// enable camera menu		
		ui->menu_Camera->setDisabled(false);
	}
	catch (DeviceException &e) {
		int ret = QMessageBox::critical(this, "Camera Initialization Failed", e.what(), QMessageBox::Close | QMessageBox::Retry, QMessageBox::Retry);

		switch (ret) {
			case QMessageBox::Retry:
				// retry intialization
				initialization();
				break;
			case QMessageBox::Close:
				// disable camera menu
				ui->menu_Camera->setDisabled(true);
				// enable intialize action
				ui->action_Initialize->setDisabled(false);
				break;
		}
	}
	catch (OpenNIException &e) {
		QMessageBox::critical(this, "OpenNI Initialization", e.what(), QMessageBox::Close, QMessageBox::Close);
		// Exit application
		QCoreApplication::exit();
	}

	// send status bar output
	ui->statusBar->showMessage("Initialization Process finished.");
}

/**
 * Is called when Initialize Menu-Item is called. Initializes the Kinect and the Camera timer.
 */
void HumanTrackerMapper::on_action_Initialize_triggered() {
	// initialize
	initialization();
}

/**
 * Destructor.
 */
HumanTrackerMapper::~HumanTrackerMapper()
{
	delete ui;
}

/**
 * Close-Event for mainwindow. Stops all processes.
 *
 * @param event Close-Event
 */
void HumanTrackerMapper::closeEvent(QCloseEvent *event)
{
	ui->statusBar->showMessage("Ending...");

	// stop timer
	if (timerIdentifier) {
		killTimer(timerIdentifier);
		timerIdentifier = 0;
	}
	
	// clean up kinect Device 
	if (kinect != NULL)
	{
		delete kinect;
	}

	event->accept();
}

/**
 * Refreshes connected Cameras.
 */
void HumanTrackerMapper::update_Devices()
{
	/* Clear camera menu */
	ui->menu_Camera->clear();

	for (string name : kinect->getCameras()) {
		QAction * action = new QAction();
		action->setText(QString(name.c_str()));
		ui->menu_Camera->addAction(action);
	}
}

/**
 * Menu item: Change Camera.
 */
void HumanTrackerMapper::changeCam() {
	/*QAction *action = qobject_cast<QAction *>(sender());
	if (action) {
		int id = action->data().toInt();
		currentCam = id;

		/* Alert-window */
		/*	QString camText = "<b>Kamera</b>: " +
				QString::fromStdString(cameras[id].getName()) +
				" (id: " + QString::number(currentCam) + ")";

			QMessageBox::information(this, "Kamera-Wechsel", camText);
			ui->statusBar->showMessage(tr("Camera-Change successful"));
			/* Change camera */
			/*	startCamera(currentCam);
			}
			else {
				ui->statusBar->showMessage(tr("Error on Change"));
			}*/
}

/**
 * Is called when Menu item About was clicked. Shows some general information.
 */
void HumanTrackerMapper::on_action_About_triggered() {
	/* About information */
	QString aboutText = "<b>HumanTrackerMapper</b><br>\
						Authors: Pascal Kruchen, Jonas Sorgenfrei<br>\
						2018/19<br><br>\
						OpenCV<br>\
						Qt<br>\
						OpenNI 2<br>\
						OpenFreenect2";

	QMessageBox::information(this, "About", aboutText);
}

/**
 * Helper function. Saves current Depth and RGB Image with ascending filenames.
 */
void HumanTrackerMapper::on_action_SaveImages_clicked() {
	/* Save current stream(s) */
	// default path
	std::string path = "Ressources/SavedImages/";

	// mats to save
	cv::Mat colorMat;
	cv::Mat depthMat;
	cv::Mat irMat; 

	// copy streams to mats
	kinect->getColorMat().copyTo(colorMat);
	kinect->getDepthMat().copyTo(depthMat);
	kinect->getIRMat().copyTo(irMat);

	bool ok;
	
	// name input dialog
	QString text = QInputDialog::getText(this, tr("QInputDialog::getText()"),
		tr("File Name:"), QLineEdit::Normal,
		"<file>", &ok);

	if (ok && !text.isEmpty()) {
		text = text.toLower();
		
		string savingNameColor = path + text.toStdString() + ".png";
		string savingNameDepth = path + text.toStdString() + "_d.png";
		string savingNameIR = path + text.toStdString() + "_ir.png";


		imwrite(savingNameColor, colorMat) ? ui->statusBar->showMessage("Color saved") : ui->statusBar->showMessage("Color saved failed");
		imwrite(savingNameDepth, depthMat) ? ui->statusBar->showMessage("Depth saved") : ui->statusBar->showMessage("Depth saved failed");
		imwrite(savingNameIR, irMat) ? ui->statusBar->showMessage("IR saved") : ui->statusBar->showMessage("IR saved failed");

	} 
}

/* ----------------------- Private ----------------------- */

#if KINECT_OUPUT_DEBUG
/**
 * Debug OpenCV Mouse Callback Function for 2D Pixel position determination
 */
void mouse_callback(int  event, int  x, int  y, int  flag, void *param)
{
	std::string windowName = *((std::string*)param);

	if (event == EVENT_LBUTTONDOWN) {
		cout << windowName << "(" << x << ", " << y << ")" << endl;
	}
}
#endif // !KINECT_OUPUT_DEBUG

/**
 * Eventhandler Keyboard Events
 */
void HumanTrackerMapper::keyPressEvent(QKeyEvent * event)
{
	/* Keyboard Input */
	
	if (ui->tabWidget->currentWidget() == ui->mapping) {

		/* Keyboard Input */
#if KINECT_OUPUT_DEBUG
		if (event->key() == Qt::Key_N) {
			ui->mappingWidget->fovX--;
		}
		if (event->key() == Qt::Key_M) {
			ui->mappingWidget->fovX++;
		}
		if (event->key() == Qt::Key_O) {
			ui->mappingWidget->fovY--;
		}
		if (event->key() == Qt::Key_P) {
			ui->mappingWidget->fovY++;
		}

		printf("%f, %f\n", ui->mappingWidget->fovX, ui->mappingWidget->fovY);

		// Camera
		if (event->key() == Qt::Key_H) {
			QVector3D pos = ui->mappingWidget->cam->Position;
			pos.setX(pos.x() - 0.001);
			ui->mappingWidget->cam->setPosition(pos);
			printf("camX %f\n", pos.x());
		}
		if (event->key() == Qt::Key_K) {
			QVector3D pos = ui->mappingWidget->cam->Position;
			pos.setX(pos.x() + 0.001);
			ui->mappingWidget->cam->setPosition(pos);
			printf("camX %f\n", pos.x());
		}

		if (event->key() == Qt::Key_U) {
			QVector3D pos = ui->mappingWidget->cam->Position;
			pos.setY(pos.y() + 0.001);
			ui->mappingWidget->cam->setPosition(pos);
			printf("camY %f\n", pos.y());
		}
		if (event->key() == Qt::Key_J) {
			QVector3D pos = ui->mappingWidget->cam->Position;
			pos.setY(pos.y() - 0.001);
			ui->mappingWidget->cam->setPosition(pos);
			printf("camY %f\n", pos.y());
		}
#endif // !KINECT_OUPUT_DEBUG
	}

#if USE_KINECT_EMU
	if (event->key() == Qt::Key_1) {
		std::string fileName = "sample_22"; //alter: 1
		// create Kinect emulator device
		kinect->~Kinect_EMU();
		kinect = new Kinect_EMU(fileName);
	}
	if (event->key() == Qt::Key_2) {
		std::string fileName = "sample_21"; //alter: 2
		// create Kinect emulator device
		kinect->~Kinect_EMU();
		kinect = new Kinect_EMU(fileName);
	}
	if (event->key() == Qt::Key_3) {
		std::string fileName = "sample_20"; //alter: 3
		// create Kinect emulator device
		kinect->~Kinect_EMU();
		kinect = new Kinect_EMU(fileName);
	}
	if (event->key() == Qt::Key_4) {
		std::string fileName = "sample_19"; // alter: 4
		// create Kinect emulator device
		kinect->~Kinect_EMU();
		kinect = new Kinect_EMU(fileName);
	}
	if (event->key() == Qt::Key_5) {
		std::string fileName = "sample_14"; // alter: 6
		// create Kinect emulator device
		kinect->~Kinect_EMU();
		kinect = new Kinect_EMU(fileName);
	}
	if (event->key() == Qt::Key_6) {
		std::string fileName = "sample_14"; // alter: 7
		// create Kinect emulator device
		kinect->~Kinect_EMU();
		kinect = new Kinect_EMU(fileName);
	}
	if (event->key() == Qt::Key_7) {
		std::string fileName = "sample_10";
		// create Kinect emulator device
		kinect->~Kinect_EMU();
		kinect = new Kinect_EMU(fileName);
	}
	if (event->key() == Qt::Key_8) {
		std::string fileName = "sample_01";
		// create Kinect emulator device
		kinect->~Kinect_EMU();
		kinect = new Kinect_EMU(fileName);
	}
	if (event->key() == Qt::Key_9) {
		std::string fileName = "sample_03";
		// create Kinect emulator device
		kinect->~Kinect_EMU();
		kinect = new Kinect_EMU(fileName);
	}
#endif // !USE_KINECT_EMU
}

void helperAddPoint(pair<vector<float>, vector<float>> * array, cv::Point3f pos, cv::Point3f col) {
	array->first.push_back(pos.x);
	array->first.push_back(pos.y);
	array->first.push_back(-pos.z);

	array->second.push_back(col.x);
	array->second.push_back(col.y);
	array->second.push_back(col.z);
}

/**
 * Update timer draws Camera images on window.
 *
 * @param QTimerEvent Timer
 */
void HumanTrackerMapper::timerEvent(QTimerEvent*)
{

	/* image data */
	cv::Mat depthD16;	// 16 bit depth
	cv::Mat irD16;		// 16 bit depth
	cv::Mat irD8;		// 8 bit infrared
	cv::Mat colorD;		// 8 bit color

	/* for Debug */
#if KINECT_OUPUT_DEBUG
	cv::Mat depthD8;	// 8 bit depth
#endif // !KINECT_OUPUT_DEBUG

	QWidget * currentWidget = ui->tabWidget->currentWidget();
	/* TODO: Check Crash on Live Image !! */
	try {
		/*
		 * Read Inputs
		 */
		{
			/*
			 * IR data
			 */
			irD16 = kinect->getIRMat();
			irD16.convertTo(irD8, CV_8U, 0.00390625);

			/*
			 * Color data
			 */
			colorD = kinect->getColorMat();

			/*
			 * Depth data
			 */
			depthD16 = kinect->getDepthMat();
			/* for Debug */
#if KINECT_OUPUT_DEBUG
			depthD16.convertTo(depthD8, CV_8U, 0.00390625);
#endif // !KINECT_OUPUT_DEBUG
		}

		/*
		 * Parameters
		 */
		bool faceRegionFound = false;
		Rect faceRegion;

		/*
		 * Tracking & Detection 
		 */
		{
			cv::Mat visualization = irD8.clone();
			// reset facemarks found 
			facemarks->setFound(false);

			//Visualization of FaceDetector can be changed in the HumanTrackerMapper Constructor (line 17 ff)
			faceRegion = faceRegionDetector.detectFaceRegion(irD8, visualization, faceRegionFound, facemarks);
			
#if KINECT_OUPUT_DEBUG
			// ------------ RGB
			std::string * windowNameRGB = new std::string("Input RGB");

			cv::namedWindow(*windowNameRGB, WINDOW_NORMAL);
			cv::imshow(*windowNameRGB, colorD);
			cv::resizeWindow(*windowNameRGB, colorD.cols / 2, colorD.rows / 2);
			setMouseCallback(*windowNameRGB, mouse_callback, (void*)(windowNameRGB));

			// ------------ Visualisation 
			cv::namedWindow("Input and Markings", WINDOW_NORMAL);
			cv::imshow("Input and Markings", visualization);
			cv::resizeWindow("Input and Markings", visualization.cols / 2, visualization.rows / 2);

			// ------------ Depth
			std::string * windowNameDepth = new std::string("Input Depth");
			cv::namedWindow(*windowNameDepth, WINDOW_NORMAL);

			cv::imshow(*windowNameDepth, depthD8);
			setMouseCallback(*windowNameDepth, mouse_callback, (void*)(windowNameDepth));

			// ------------ IR
			std::string * windowNameIR = new std::string("Input IR");
			cv::namedWindow(*windowNameIR, WINDOW_NORMAL);
			cv::imshow(*windowNameIR, irD8);
			setMouseCallback(*windowNameIR, mouse_callback, (void*)(windowNameIR));

			// ------------ cropped IR mat to detected head region 
			cv::namedWindow("IR Cropped To ROI", WINDOW_NORMAL);

			// ------------ cropped Depth mat to detected head region with binary map
			cv::namedWindow("Binary Mask", WINDOW_NORMAL);

#endif // !KINECT_OUPUT_DEBUG
		
			if (faceRegionFound) {
				// ------------ cropped Depth mat to detected head region
				
				//Make FaceRegion larger, to avoid dataloss of face
				cv::Rect largeFaceRegion = faceRegion;
				float faceScaleFactor = 0.5f;
				cv::Size scaledSize(faceRegion.width * faceScaleFactor, faceRegion.height * faceScaleFactor);
				cv::Point offset(scaledSize.width / 2, scaledSize.height / 2);
				largeFaceRegion += scaledSize;
				largeFaceRegion -= offset;
				largeFaceRegion.x = (largeFaceRegion.x >= 0) ? largeFaceRegion.x : 0;
				largeFaceRegion.y = (largeFaceRegion.y >= 0) ? largeFaceRegion.y : 0;
				if (largeFaceRegion.x + largeFaceRegion.width >= depthD16.cols) {
					largeFaceRegion.width = depthD16.cols - largeFaceRegion.x - 1;
				}
				if (largeFaceRegion.y + largeFaceRegion.height >= depthD16.rows) {
					largeFaceRegion.height = depthD16.rows - largeFaceRegion.y - 1;
				}
				cv::Mat croppedDepthImageLarge(depthD16, largeFaceRegion);

				//Check if Landmarks are in detected region and valid, otherwise take last average face depth for binary mask
				const float ALLOWED_OUTSIDE_LANDMARKS = 0.1; //Percent
				bool calcAverageFaceDepth = facemarks->foundLandmarks(); //if no landmarks found, use last depth
				if (calcAverageFaceDepth) {
					std::vector<cv::Point2f> landmarks = facemarks->getFaceLandmarks();
					int outsideLandmarks = 0, totalLandmarks = 0;
					for (const auto& landmark : landmarks) {
						if (landmark.x < faceRegion.x || landmark.x > faceRegion.x + faceRegion.width 
							|| landmark.y < faceRegion.y || landmark.y > faceRegion.y + faceRegion.height) {
							//landmark is outside
							outsideLandmarks++;
						}
						totalLandmarks++;
					}
					calcAverageFaceDepth = totalLandmarks * ALLOWED_OUTSIDE_LANDMARKS >= outsideLandmarks; //if too many landmarks are outside of region, use last depth
				}

				//Create binary mask
				cv::Mat binaryMask = createBinaryMask(depthD16, facemarks, faceRegion, croppedDepthImageLarge, calcAverageFaceDepth);
				
				/*
				 * Head Position 
				 */
				std::vector<std::vector<cv::Point> > contours;
				std::vector<cv::Vec4i> hierarchy;
				
				//Cut off body with ellipse shape
				// Find contours
				cv::findContours(binaryMask, contours, hierarchy, CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE, cv::Point(0, 0));
				
				/* Method 1 */
				
				cv::Point2f noseCenter = facemarks->getLandmark(facemarks->FLM_NOSE_DORSUM_TOP);
				if (contours.size() > 0) {
					int largest_area = 0;
					int largest_contour_index = 0;
					Rect bounding_rect;

					for (int i = 0; i < contours.size(); i++) // iterate through each contour. 
					{
						double a = contourArea(contours[i], false);  //  Find the area of contour
						if (a > largest_area) {
							largest_area = a;
							largest_contour_index = i;                //Store the index of largest contour
							bounding_rect = boundingRect(contours[i]); // Find the bounding rectangle for biggest contour
						}
					}

					cv::Moments m = cv::moments(contours[largest_contour_index]);
					float cX = int(m.m10 / m.m00);
					float cY = int(m.m01 / m.m00);

					noseCenter = cv::Point2f(largeFaceRegion.x+cX, largeFaceRegion.y + cY);
				}
								
				cv::Point3f avWordPos;
				if (noseCenter.y < 0) {
					noseCenter.y = 0;
				}
				if (noseCenter.x < 0) {
					noseCenter.x = 0;
				}
				if (noseCenter.y > depthD16.rows) {
					noseCenter.y = depthD16.rows - 1;
				}
				if (noseCenter.x > depthD16.cols) {
					noseCenter.x = depthD16.cols - 1;
				}

				cv::Point3f noseWorldCoordinate = kinect->depthToWorld(noseCenter,
					(float)depthD16.at<unsigned short>(noseCenter.y, noseCenter.x));
				
				/*
				 * Head Rotation
				 */

				 /*
				  * Debug Lines for 3D Viewer 
				  * first - Point World Pos
				  * second - Point
				  */
				pair<vector<float>, vector<float>> linePoints;

				cv::Vec3f rotationvec = Vec3f(0,0,-1);
				
				if (calcAverageFaceDepth) {
					const float visVectorLength = 0.1;
					int countDiv = 0;

					cv::Vec3f dirVec;
					cv::Point2f eyeBLL = facemarks->getLandmark(facemarks->FLM_EYE_BROWN_LEFT_LEFT);
					cv::Point2f eyeBL = facemarks->getLandmark(facemarks->FLM_EYE_BROWN_LEFT_CENTER);
					cv::Point2f eyeBLR = facemarks->getLandmark(facemarks->FLM_EYE_BROWN_LEFT_RIGHT);

					cv::Point2f eyeBRL = facemarks->getLandmark(facemarks->FLM_EYE_BROWN_RIGHT_CENTER);
					cv::Point2f eyeBR = facemarks->getLandmark(facemarks->FLM_EYE_BROWN_RIGHT_CENTER);
					cv::Point2f eyeBRR = facemarks->getLandmark(facemarks->FLM_EYE_BROWN_RIGHT_CENTER);


					cv::Point2f eyeLL = facemarks->getLandmark(facemarks->FLM_EYE_LEFT_CORNER_LEFT);
					cv::Point2f eyeLR = facemarks->getLandmark(facemarks->FLM_EYE_LEFT_CORNER_RIGHT);

					cv::Point2f eyeRL = facemarks->getLandmark(facemarks->FLM_EYE_RIGHT_CORNER_LEFT);
					cv::Point2f eyeRR = facemarks->getLandmark(facemarks->FLM_EYE_RIGHT_CORNER_RIGHT);

					cv::Point2f mouthT = facemarks->getLandmark(facemarks->FLM_MOUTH_TOP);
					cv::Point2f mouthB = facemarks->getLandmark(facemarks->FLM_MOUTH_BOTTOM);

					cv::Point2f mouthL = facemarks->getLandmark(facemarks->FLM_MOUTH_CORNER_LEFT);
					cv::Point2f mouthR = facemarks->getLandmark(facemarks->FLM_MOUTH_CORNER_RIGHT);
					
					cv::Point2f philitrumC = facemarks->getLandmark(facemarks->FLM_PHILITRUM_CENTER);
					
					cv::Point2f noseBL = facemarks->getLandmark(facemarks->FLM_NOSE_BOTTOM_LEFT);
					cv::Point2f noseBR = facemarks->getLandmark(facemarks->FLM_NOSE_BOTTOM_RIGHT);
					cv::Point2f noseTip = facemarks->getLandmark(facemarks->FLM_NOSE_TIP);


					if (binaryMask.at<uchar>(eyeBL.y - largeFaceRegion.y, eyeBL.x - largeFaceRegion.x) != 0 &&
						binaryMask.at<uchar>(eyeBR.y - largeFaceRegion.y, eyeBR.x - largeFaceRegion.x) != 0 &&
						binaryMask.at<uchar>(noseTip.y - largeFaceRegion.y, noseTip.x - largeFaceRegion.x) != 0) {

						cv::Point3f eyeBLW = kinect->depthToWorld(eyeBL, (float)depthD16.at<unsigned short>(eyeBL.y, eyeBL.x));
						cv::Point3f eyeBRW = kinect->depthToWorld(eyeBR, (float)depthD16.at<unsigned short>(eyeBR.y, eyeBR.x));
						cv::Point3f noseTipW = kinect->depthToWorld(noseTip, (float)depthD16.at<unsigned short>(noseTip.y, noseTip.x));

						cv::Vec3f vec1 = eyeBLW - noseTipW;
						cv::Vec3f vec2 = eyeBRW - noseTipW;

						cv::Vec3f vec3 = cv::normalize(vec2.cross(vec1));

						cv::Point3f noseTipVis = (cv::Vec3f)noseTipW + vec3 * visVectorLength;

						helperAddPoint(&linePoints, eyeBLW, cv::Point3f(0, 1, 0));
						helperAddPoint(&linePoints, noseTipW, cv::Point3f(0, 1, 0));

						helperAddPoint(&linePoints, eyeBRW, cv::Point3f(0, 1, 0));
						helperAddPoint(&linePoints, noseTipW, cv::Point3f(0, 1, 0));

						helperAddPoint(&linePoints, noseTipW, cv::Point3f(1, 0, 0));
						helperAddPoint(&linePoints, noseTipVis, cv::Point3f(1, 0, 0));

						dirVec += vec3;
						countDiv++;
					}

					if (binaryMask.at<uchar>(mouthL.y - largeFaceRegion.y, mouthR.x - largeFaceRegion.x) != 0 &&
						binaryMask.at<uchar>(mouthR.y - largeFaceRegion.y, mouthR.x - largeFaceRegion.x) != 0 &&
						binaryMask.at<uchar>(philitrumC.y - largeFaceRegion.y, philitrumC.x - largeFaceRegion.x) != 0) {
						cv::Point3f mouthLW = kinect->depthToWorld(mouthL, (float)depthD16.at<unsigned short>(mouthL.y, mouthL.x));
						cv::Point3f mouthRW = kinect->depthToWorld(mouthR, (float)depthD16.at<unsigned short>(mouthR.y, mouthR.x));
						cv::Point3f philitrumCW = kinect->depthToWorld(philitrumC, (float)depthD16.at<unsigned short>(philitrumC.y, philitrumC.x));
						
						cv::Vec3f vec1 = mouthRW - philitrumCW;
						cv::Vec3f vec2 = mouthLW - philitrumCW;

						cv::Vec3f vec3 = cv::normalize(vec2.cross(vec1));

						cv::Point3f philitrumCVis = (cv::Vec3f)philitrumCW + vec3 * visVectorLength;

						helperAddPoint(&linePoints, mouthLW, cv::Point3f(0, 1, 0));
						helperAddPoint(&linePoints, philitrumCW, cv::Point3f(0, 1, 0));

						helperAddPoint(&linePoints, mouthRW, cv::Point3f(0, 1, 0));
						helperAddPoint(&linePoints, philitrumCW, cv::Point3f(0, 1, 0));

						helperAddPoint(&linePoints, philitrumCW, cv::Point3f(1, 0, 0));
						helperAddPoint(&linePoints, philitrumCVis, cv::Point3f(1, 0, 0));

						dirVec += vec3;
						countDiv++;
					}

					if (binaryMask.at<uchar>(eyeLL.y - largeFaceRegion.y, eyeLL.x - largeFaceRegion.x) != 0 &&
						binaryMask.at<uchar>(eyeLR.y - largeFaceRegion.y, eyeLR.x - largeFaceRegion.x) != 0 &&
						binaryMask.at<uchar>(mouthL.y - largeFaceRegion.y, mouthL.x - largeFaceRegion.x) != 0) {
						cv::Point3f eyeLLW = kinect->depthToWorld(eyeLL, (float)depthD16.at<unsigned short>(eyeLL.y, eyeLL.x));
						cv::Point3f eyeLRW = kinect->depthToWorld(eyeLR, (float)depthD16.at<unsigned short>(eyeLR.y, eyeLR.x));
						cv::Point3f mouthLW = kinect->depthToWorld(mouthL, (float)depthD16.at<unsigned short>(mouthL.y, mouthL.x));

						cv::Vec3f vec1 = eyeLLW - mouthLW;
						cv::Vec3f vec2 = eyeLRW - mouthLW;

						cv::Vec3f vec3 = cv::normalize(vec2.cross(vec1));

						cv::Point3f  mouthVis = (cv::Vec3f)mouthLW + vec3 * visVectorLength;

						helperAddPoint(&linePoints, mouthLW, cv::Point3f(0, 1, 0));
						helperAddPoint(&linePoints, eyeLLW, cv::Point3f(0, 1, 0));

						helperAddPoint(&linePoints, mouthLW, cv::Point3f(0, 1, 0));
						helperAddPoint(&linePoints, eyeLRW, cv::Point3f(0, 1, 0));

						helperAddPoint(&linePoints, mouthLW, cv::Point3f(1, 0, 0));
						helperAddPoint(&linePoints, mouthVis, cv::Point3f(1, 0, 0));

						dirVec += vec3;
						countDiv++;
					}

					if (binaryMask.at<uchar>(eyeRL.y - largeFaceRegion.y, eyeRL.x - largeFaceRegion.x) != 0 &&
						binaryMask.at<uchar>(eyeRR.y - largeFaceRegion.y, eyeRR.x - largeFaceRegion.x) != 0 &&
						binaryMask.at<uchar>(mouthR.y - largeFaceRegion.y, mouthR.x - largeFaceRegion.x) != 0) {
						cv::Point3f eyeRLW = kinect->depthToWorld(eyeRL, (float)depthD16.at<unsigned short>(eyeRL.y, eyeRL.x));
						cv::Point3f eyeRRW = kinect->depthToWorld(eyeRR, (float)depthD16.at<unsigned short>(eyeRR.y, eyeRR.x));
						cv::Point3f mouthRW = kinect->depthToWorld(mouthR, (float)depthD16.at<unsigned short>(mouthR.y, mouthR.x));

						cv::Vec3f vec1 = eyeRLW - mouthRW;
						cv::Vec3f vec2 = eyeRRW - mouthRW;

						cv::Vec3f vec3 = cv::normalize(vec2.cross(vec1));

						cv::Point3f  mouthVis = (cv::Vec3f)mouthRW + vec3 * visVectorLength;

						helperAddPoint(&linePoints, mouthRW, cv::Point3f(0, 1, 0));
						helperAddPoint(&linePoints, eyeRLW, cv::Point3f(0, 1, 0));

						helperAddPoint(&linePoints, mouthRW, cv::Point3f(0, 1, 0));
						helperAddPoint(&linePoints, eyeRRW, cv::Point3f(0, 1, 0));

						helperAddPoint(&linePoints, mouthRW, cv::Point3f(1, 0, 0));
						helperAddPoint(&linePoints, mouthVis, cv::Point3f(1, 0, 0));

						dirVec += vec3;
						countDiv++;
					}

					if (binaryMask.at<uchar>(mouthT.y - largeFaceRegion.y, mouthT.x - largeFaceRegion.x) != 0 &&
						binaryMask.at<uchar>(noseBR.y - largeFaceRegion.y, noseBR.x - largeFaceRegion.x) != 0 &&
						binaryMask.at<uchar>(mouthR.y - largeFaceRegion.y, mouthR.x - largeFaceRegion.x) != 0) {
						cv::Point3f mouthTW = kinect->depthToWorld(mouthT, (float)depthD16.at<unsigned short>(mouthT.y, mouthT.x));
						cv::Point3f noseBRW = kinect->depthToWorld(noseBR, (float)depthD16.at<unsigned short>(noseBR.y, noseBR.x));
						cv::Point3f mouthRW = kinect->depthToWorld(mouthR, (float)depthD16.at<unsigned short>(mouthR.y, mouthR.x));

						cv::Vec3f vec1 = mouthTW - mouthRW;
						cv::Vec3f vec2 = noseBRW - mouthRW;

						cv::Vec3f vec3 = cv::normalize(vec2.cross(vec1));

						cv::Point3f  mouthVis = (cv::Vec3f)mouthRW + vec3 * visVectorLength;

						helperAddPoint(&linePoints, mouthTW, cv::Point3f(0, 1, 0));
						helperAddPoint(&linePoints, mouthRW, cv::Point3f(0, 1, 0));

						helperAddPoint(&linePoints, noseBRW, cv::Point3f(0, 1, 0));
						helperAddPoint(&linePoints, mouthRW, cv::Point3f(0, 1, 0));

						helperAddPoint(&linePoints, mouthRW, cv::Point3f(1, 0, 0));
						helperAddPoint(&linePoints, mouthVis, cv::Point3f(1, 0, 0));

						dirVec += vec3;
						countDiv++;
					}
				
					if (binaryMask.at<uchar>(mouthT.y - largeFaceRegion.y, mouthT.x - largeFaceRegion.x) != 0 &&
						binaryMask.at<uchar>(noseBL.y - largeFaceRegion.y, noseBL.x - largeFaceRegion.x) != 0 &&
						binaryMask.at<uchar>(mouthL.y - largeFaceRegion.y, mouthL.x - largeFaceRegion.x) != 0) {
						cv::Point3f mouthTW = kinect->depthToWorld(mouthT, (float)depthD16.at<unsigned short>(mouthT.y, mouthT.x));
						cv::Point3f noseBLW = kinect->depthToWorld(noseBL, (float)depthD16.at<unsigned short>(noseBL.y, noseBL.x));
						cv::Point3f mouthLW = kinect->depthToWorld(mouthL, (float)depthD16.at<unsigned short>(mouthL.y, mouthL.x));

						cv::Vec3f vec1 = noseBLW - mouthLW;
						cv::Vec3f vec2 = mouthTW - mouthLW;

						cv::Vec3f vec3 = cv::normalize(vec2.cross(vec1));

						cv::Point3f  mouthVis = (cv::Vec3f)mouthLW + vec3 * visVectorLength;

						helperAddPoint(&linePoints, mouthTW, cv::Point3f(0, 1, 0));
						helperAddPoint(&linePoints, mouthLW, cv::Point3f(0, 1, 0));

						helperAddPoint(&linePoints, noseBLW, cv::Point3f(0, 1, 0));
						helperAddPoint(&linePoints, mouthLW, cv::Point3f(0, 1, 0));

						helperAddPoint(&linePoints, mouthLW, cv::Point3f(1, 0, 0));
						helperAddPoint(&linePoints, mouthVis, cv::Point3f(1, 0, 0));

						dirVec += vec3;
						countDiv++;
					}

					if (binaryMask.at<uchar>(eyeRL.y - largeFaceRegion.y, eyeRL.x - largeFaceRegion.x) != 0 &&
						binaryMask.at<uchar>(eyeRR.y - largeFaceRegion.y, eyeRR.x - largeFaceRegion.x) != 0 &&
						binaryMask.at<uchar>(mouthR.y - largeFaceRegion.y, mouthR.x - largeFaceRegion.x) != 0) {
						cv::Point3f eyeRLW = kinect->depthToWorld(eyeRL, (float)depthD16.at<unsigned short>(eyeRL.y, eyeRL.x));
						cv::Point3f eyeRRW = kinect->depthToWorld(eyeRR, (float)depthD16.at<unsigned short>(eyeRR.y, eyeRR.x));
						cv::Point3f mouthRW = kinect->depthToWorld(mouthR, (float)depthD16.at<unsigned short>(mouthR.y, mouthR.x));

						cv::Vec3f vec1 = eyeRLW - mouthRW;
						cv::Vec3f vec2 = eyeRRW - mouthRW;

						cv::Vec3f vec3 = cv::normalize(vec2.cross(vec1));

						cv::Point3f  mouthVis = (cv::Vec3f)mouthRW + vec3 * visVectorLength;

						helperAddPoint(&linePoints, eyeRLW, cv::Point3f(0, 1, 0));
						helperAddPoint(&linePoints, mouthRW, cv::Point3f(0, 1, 0));

						helperAddPoint(&linePoints, eyeRRW, cv::Point3f(0, 1, 0));
						helperAddPoint(&linePoints, mouthRW, cv::Point3f(0, 1, 0));

						helperAddPoint(&linePoints, mouthRW, cv::Point3f(1, 0, 0));
						helperAddPoint(&linePoints, mouthVis, cv::Point3f(1, 0, 0));

						dirVec += vec3;
						countDiv++;
					}

					if (binaryMask.at<uchar>(eyeLL.y - largeFaceRegion.y, eyeLL.x - largeFaceRegion.x) != 0 &&
						binaryMask.at<uchar>(eyeLR.y - largeFaceRegion.y, eyeLR.x - largeFaceRegion.x) != 0 &&
						binaryMask.at<uchar>(mouthL.y - largeFaceRegion.y, mouthL.x - largeFaceRegion.x) != 0) {
						cv::Point3f eyeLLW = kinect->depthToWorld(eyeLL, (float)depthD16.at<unsigned short>(eyeLL.y, eyeLL.x));
						cv::Point3f eyeLRW = kinect->depthToWorld(eyeLR, (float)depthD16.at<unsigned short>(eyeLR.y, eyeLR.x));
						cv::Point3f mouthLW = kinect->depthToWorld(mouthL, (float)depthD16.at<unsigned short>(mouthL.y, mouthL.x));

						cv::Vec3f vec1 = eyeLLW - mouthLW;
						cv::Vec3f vec2 = eyeLRW - mouthLW;

						cv::Vec3f vec3 = cv::normalize(vec2.cross(vec1));

						cv::Point3f  mouthVis = (cv::Vec3f)mouthLW + vec3 * visVectorLength;

						helperAddPoint(&linePoints, eyeLLW, cv::Point3f(0, 1, 0));
						helperAddPoint(&linePoints, mouthLW, cv::Point3f(0, 1, 0));

						helperAddPoint(&linePoints, eyeLRW, cv::Point3f(0, 1, 0));
						helperAddPoint(&linePoints, mouthLW, cv::Point3f(0, 1, 0));

						helperAddPoint(&linePoints, mouthLW, cv::Point3f(1, 0, 0));
						helperAddPoint(&linePoints, mouthVis, cv::Point3f(1, 0, 0));

						dirVec += vec3;
						countDiv++;
					}

					if (binaryMask.at<uchar>(eyeBLL.y - largeFaceRegion.y, eyeBLL.x - largeFaceRegion.x) != 0 &&
						binaryMask.at<uchar>(eyeBLR.y - largeFaceRegion.y, eyeBLR.x - largeFaceRegion.x) != 0 &&
						binaryMask.at<uchar>(noseTip.y - largeFaceRegion.y, noseTip.x - largeFaceRegion.x) != 0) {
						cv::Point3f eyeBLLW = kinect->depthToWorld(eyeBLL, (float)depthD16.at<unsigned short>(eyeBLL.y, eyeBLL.x));
						cv::Point3f eyeBLRW = kinect->depthToWorld(eyeBLR, (float)depthD16.at<unsigned short>(eyeBLR.y, eyeBLR.x));
						cv::Point3f noseTipW = kinect->depthToWorld(noseTip, (float)depthD16.at<unsigned short>(noseTip.y, noseTip.x));

						cv::Vec3f vec1 = eyeBLLW - noseTipW;
						cv::Vec3f vec2 = eyeBLRW - noseTipW;

						cv::Vec3f vec3 = cv::normalize(vec2.cross(vec1));

						cv::Point3f  noseVis = (cv::Vec3f)noseTipW + vec3 * visVectorLength;

						helperAddPoint(&linePoints, eyeBLLW, cv::Point3f(0, 1, 0));
						helperAddPoint(&linePoints, noseTipW, cv::Point3f(0, 1, 0));

						helperAddPoint(&linePoints, eyeBLRW, cv::Point3f(0, 1, 0));
						helperAddPoint(&linePoints, noseTipW, cv::Point3f(0, 1, 0));

						helperAddPoint(&linePoints, noseTipW, cv::Point3f(1, 0, 0));
						helperAddPoint(&linePoints, noseVis, cv::Point3f(1, 0, 0));

						dirVec += vec3;
						countDiv++;
					}

					if (binaryMask.at<uchar>(eyeBRL.y - largeFaceRegion.y, eyeBLL.x - largeFaceRegion.x) != 0 &&
						binaryMask.at<uchar>(eyeBRR.y - largeFaceRegion.y, eyeBRR.x - largeFaceRegion.x) != 0 &&
						binaryMask.at<uchar>(noseTip.y - largeFaceRegion.y, noseTip.x - largeFaceRegion.x) != 0) {
						cv::Point3f eyeBRLW = kinect->depthToWorld(eyeBRL, (float)depthD16.at<unsigned short>(eyeBRL.y, eyeBRL.x));
						cv::Point3f eyeBRRW = kinect->depthToWorld(eyeBRR, (float)depthD16.at<unsigned short>(eyeBRR.y, eyeBRR.x));
						cv::Point3f noseTipW = kinect->depthToWorld(noseTip, (float)depthD16.at<unsigned short>(noseTip.y, noseTip.x));

						cv::Vec3f vec1 = eyeBRLW - noseTipW;
						cv::Vec3f vec2 = eyeBRRW - noseTipW;

						cv::Vec3f vec3 = cv::normalize(vec2.cross(vec1));

						cv::Point3f  noseVis = (cv::Vec3f)noseTipW + vec3 * visVectorLength;

						helperAddPoint(&linePoints, eyeBRLW, cv::Point3f(0, 1, 0));
						helperAddPoint(&linePoints, noseTipW, cv::Point3f(0, 1, 0));

						helperAddPoint(&linePoints, eyeBRRW, cv::Point3f(0, 1, 0));
						helperAddPoint(&linePoints, noseTipW, cv::Point3f(0, 1, 0));

						helperAddPoint(&linePoints, noseTipW, cv::Point3f(1, 0, 0));
						helperAddPoint(&linePoints, noseVis, cv::Point3f(1, 0, 0));

						dirVec += vec3;
						countDiv++;
					}
				
					if (binaryMask.at<uchar>(noseBL.y - largeFaceRegion.y, noseBL.x - largeFaceRegion.x) != 0 &&
						binaryMask.at<uchar>(noseBR.y - largeFaceRegion.y, noseBR.x - largeFaceRegion.x) != 0 &&
						binaryMask.at<uchar>(mouthB.y - largeFaceRegion.y, mouthB.x - largeFaceRegion.x) != 0) {
						cv::Point3f noseBLW = kinect->depthToWorld(noseBL, (float)depthD16.at<unsigned short>(noseBL.y, noseBL.x));
						cv::Point3f noseBRW = kinect->depthToWorld(noseBR, (float)depthD16.at<unsigned short>(noseBR.y, noseBR.x));
						cv::Point3f mouthBW = kinect->depthToWorld(mouthB, (float)depthD16.at<unsigned short>(mouthB.y, mouthB.x));

						cv::Vec3f vec1 = noseBLW - mouthBW;
						cv::Vec3f vec2 = noseBRW - mouthBW;

						cv::Vec3f vec3 = cv::normalize(vec2.cross(vec1));

						cv::Point3f  mouthVis = (cv::Vec3f)mouthBW + vec3 * visVectorLength;

						helperAddPoint(&linePoints, noseBLW, cv::Point3f(0, 1, 0));
						helperAddPoint(&linePoints, mouthBW, cv::Point3f(0, 1, 0));

						helperAddPoint(&linePoints, noseBRW, cv::Point3f(0, 1, 0));
						helperAddPoint(&linePoints, mouthBW, cv::Point3f(0, 1, 0));

						helperAddPoint(&linePoints, mouthBW, cv::Point3f(1, 0, 0));
						helperAddPoint(&linePoints, mouthVis, cv::Point3f(1, 0, 0));

						dirVec += vec3;
						countDiv++;
					}

					if (countDiv > 0)
					{
						rotationvec = dirVec/countDiv;
					}
				}

				noseWorldCoordinate = (cv::Vec3f) noseWorldCoordinate + cv::normalize((cv::Vec3f) noseWorldCoordinate) * 0.12f;
				cv::Point3f worldCoordinate = noseWorldCoordinate;
				/* Mapping Widget */	
				if (currentWidget == ui->mapping) {
						ui->mappingWidget->setObjectPosition(worldCoordinate.x, worldCoordinate.y, -worldCoordinate.z);
						if (ui->activateRotation->isChecked()) {
							ui->mappingWidget->setObjectRotation(rotationvec);
						}
				}

				/* Point Cloud Widget */
				if (currentWidget == ui->pointCloud) {
					//	TODO 
					// Loop over points, create vec 3 with points not 0 
					vector<float> points;

					if (ui->showBtnFull->isChecked()) {
						for (int y = 0; y < depthD16.rows; y++) {
							for (int x = 0; x < depthD16.cols; x++) {
								cv::Point2f texCoord((float)(x), (float)(y));
								float depth = (float)depthD16.at<unsigned short>(texCoord.y, texCoord.x);
								if (depth > 0) {
									cv::Point3f world = kinect->depthToWorld(texCoord, depth);
									points.push_back(world.x);
									points.push_back(world.y);
									points.push_back(-world.z);
								}
							}
						}
					}
					else {
						for (int y = 0; y < binaryMask.rows; y++) {
							for (int x = 0; x < binaryMask.cols; x++) {
								if (binaryMask.at<uchar>(y, x) == 255) {
									cv::Point2f texCoord((float)(largeFaceRegion.x + x), (float)(largeFaceRegion.y + y));
									float depth = (float)depthD16.at<unsigned short>(texCoord.y, texCoord.x);
									if (depth > 0) {
										cv::Point3f world = kinect->depthToWorld(texCoord, depth);
										points.push_back(world.x);
										points.push_back(world.y);
										points.push_back(-world.z);
									}
								}
							}
						}

					}
	
					vector<Point2f> fLM = facemarks->getFaceLandmarks();
					cv::Point3f center;
					int countDiv = 0;
					/*
					for (int i = 0; i < facemarks->FLM_COUNT_POINTS; i++) {
						if (binaryMask.at<uchar>(fLM[i].y-largeFaceRegion.y, fLM[i].x-largeFaceRegion.x) == 255) {
							cv::Point2f texCoord((float)(fLM[i].x), (float)(fLM[i].y));
							float depth = (float)depthD16.at<unsigned short>(texCoord.y, texCoord.x);
							if (depth > 0) {
								cv::Point3f world = kinect->depthToWorld(texCoord, depth);

								center += world;
								countDiv++;
								{

									linePoints.first.push_back(world.x);
									linePoints.first.push_back(world.y);
									linePoints.first.push_back(-world.z);

									linePoints.first.push_back(world.x);
									linePoints.first.push_back(world.y);
									linePoints.first.push_back(-world.z+0.01f);
								}

								{
									if (i == facemarks->FLM_NOSE_TIP) {
										linePoints.second.push_back(1.0f);
										linePoints.second.push_back(0.0f);
										linePoints.second.push_back(0.0f);

										linePoints.second.push_back(1.0f);
										linePoints.second.push_back(0.0f);
										linePoints.second.push_back(0.0f);
									}
									else {
										linePoints.second.push_back(0.0f);
										linePoints.second.push_back(1.0f);
										linePoints.second.push_back(0.0f);

										linePoints.second.push_back(0.0f);
										linePoints.second.push_back(1.0f);
										linePoints.second.push_back(0.0f);
									}

								
								}
							}
						}
					}
					*/
					
					ui->pointCloudWidget->drawPointCloud(points);
					ui->pointCloudWidget->drawVectors(linePoints.first, linePoints.second);
					ui->pointCloudWidget->setObjectPosition(worldCoordinate.x, worldCoordinate.y, -worldCoordinate.z);
					ui->pointCloudWidget->setObjectRotation(rotationvec);
				}

#if KINECT_OUPUT_DEBUG
				// ------------ cropped IR mat to detected head region 

				// region of interest on IR
				if (irD8.rows > 0 && irD8.cols > 0) {
					cv::Mat roi_ir(irD8, faceRegion);
					cv::Mat croppedIRImage;
					// crop image to new Mat
					roi_ir.copyTo(croppedIRImage);
					//circle(croppedIRImage, CvPoint(headCenter_x, headCenter_y), 1, CV_RGB(255, 255, 255), 1);

					//cv::Mat cropped = cv::Mat::zeros(croppedIRImage.size(), croppedIRImage.type());
					//cropped.copyTo(croppedIRImage, binaryMask);
					if (croppedIRImage.rows > 0 && croppedIRImage.cols > 0) {
						cv::imshow("IR Cropped To ROI", croppedIRImage);
						cv::resizeWindow("IR Cropped To ROI", 200, 200);
					}

				}
				
				
				// ------------ binary mask for face 
				if (binaryMask.rows > 0 && binaryMask.cols > 0) {
					cv::imshow("Binary Mask", binaryMask);
					cv::resizeWindow("Binary Mask", 200, 200);
				}
#endif 
			}
			else {
				/* TODO: case loosing Head*/
				// Hide Head?? 
			}

#if KINECT_OUPUT_DEBUG
			cv::waitKey(1);
#endif
		}

		/* 
		 * 
		 * Current Widget of Tab 
		 *
		 */
			/* Mapping Widget */
			if (currentWidget == ui->mapping) {
				// show Image 
				ui->mappingWidget->showImage(colorD);
			}

			/* Point Cloud Widget */
			if (currentWidget == ui->pointCloud) {
				ui->pointCloudWidget->showCam = ui->showCam->isChecked();
				ui->pointCloudWidget->showMod = ui->showModel->isChecked();
				ui->pointCloudWidget->showVecs = ui->showVecs->isChecked();
				ui->pointCloudWidget->showPC = ui->showPC->isChecked();
			
				ui->pointCloudWidget->update();
			}
	}
	catch (DeviceException &e) {


		if (timerIdentifier > 0) {
			killTimer(timerIdentifier);
			timerIdentifier = 0;
		}

		int ret = QMessageBox::critical(this, "No valid Frame found", e.what(), QMessageBox::Close | QMessageBox::Retry, QMessageBox::Retry);

		switch (ret) {
		case QMessageBox::Retry:
			initialization();
			break;
		case QMessageBox::Close:
			ui->menu_Camera->setDisabled(true);
			ui->action_Initialize->setDisabled(false);
			break;
		}
	}
	catch (Exception &e) {
		/* Catch unkown Exception */
		printf("Unknow Exception");
		std::cout << e.what();
		exit(1);
	}

	/* free memory*/
	depthD16.release();
	irD16.release();
	irD8.release();
	colorD.release();

	/* for Debug */
#if KINECT_OUPUT_DEBUG
	depthD8.release();	// 8 bit depth
#endif // !KINECT_OUPUT_DEBUG
}

