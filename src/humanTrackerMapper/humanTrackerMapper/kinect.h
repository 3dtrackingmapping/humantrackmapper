#ifndef KINECT_H
#define KINECT_H

// Assertions
#include <assert.h>

// Exceptions
#include "exception.h"

// OpenNI Includes
#include <OpenNI.h>

#include "captureInterface.h"
#include "captureFrameListener.h"

using namespace openni;

/**
 * Class for accessing the Kinect camera and initialize OpenNI.
 */
class Kinect : public CaptureInterface {
public:
	/**
	 * Constructor.
	 */
	Kinect();
	/**
	 * Destructor.
	 */
	~Kinect();

	/*
	 *
	 * Interface Implementation
	 *
	 */

	/**
	 * Delivers the color image of the Camera.
	 * @return color image
	 */
	cv::Mat getColorMat() override;
	/**
	 * Delivers the color image of the Camera.
	 * @return color image
	 */
	cv::Mat getDepthMat() override;
	/**
	 * Delivers the IR-image of the Camera.
	 * @return IR-image
	 */
	cv::Mat getIRMat() override;
	/**
	 * Converts the Depth Value to Wolrd Data
	 * @return world Coordinate xyz
	 */
	cv::Point3f Kinect::depthToWorld(cv::Point2f depthCoord, float depth) override;


	/*
	 *
	 * Public
	 *
	 */

	/**
	 * Initializes OpenNI.
	 */
	void initOpenNI();

	/**
	 * Updates all available Devices.
	 */
	void updateDevices();

	/**
	 * Initializes a single Kinect camera.
	 * @param cameraIDX Index of camera
	 */
	void initKinect(int cameraIDX);

	/**
	 * Vector of all available Cameras.
	 * @return all cameras
	 */
	std::vector<std::string> getCameras();

private:
	/* all connected camera devices. */
	std::map<int, openni::DeviceInfo> cameras;
	/** depth data stream of current camera. */
	openni::VideoStream depth;
	CaptureFrameListener depthListener;
	/** color data stream of current camera. */
	openni::VideoStream color;
	CaptureFrameListener colorListener;
	/** ir data stream of current camera. */
	openni::VideoStream ir;
	CaptureFrameListener irListener;
	/** current device / camera. */
	openni::Device device;


	CameraIntrinsics intrinsics;

	/**
	 * Deletes all VideoStreams of the camera.
	 */
	void deleteCurrentVideoStreams();
};

#endif