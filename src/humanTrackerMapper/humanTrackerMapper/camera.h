#ifndef CAMERA_H
#define CAMERA_H

#include <vector>
#include <qvector3d.h>
#include <qmatrix4x4.h>
#include <QMath.h>

/**
 * Defines several possible options for camera movement.
 * Used as abstraction to stay away from window-system
 * specific input methods
 */
enum Camera_Movement {
	FORWARD,
	BACKWARD,
	LEFT,
	RIGHT,
	UP,
	DOWN
};

// Default camera values
const float YAW = -90.0f;
const float PITCH = 0.0f;

const float SENSITIVITY = 0.05f;
const float ZOOM = 45.0f;


class Camera
{
public: 
	// Camera Attributes
	QVector3D Position;
	QVector3D Front;
	QVector3D Up;
	QVector3D Right;
	QVector3D WorldUp;
	// Focus Point
	QVector3D focus;
	//Euler Angles
	float Yaw;
	float Pitch;
	// camera options
	float MouseSensitivity;
	//Camera Options
	float Zoom;

	// Constructor with vectors
	Camera(QVector3D position = QVector3D(0.0f, 0.0f, 0.0f), QVector3D up = QVector3D(0.0f, 1.0f, 0.0f), float yaw = YAW, float pitch = PITCH) : Front(QVector3D(0.0f, 0.0f, -1.0f)), MouseSensitivity(SENSITIVITY), Zoom(ZOOM)
	{
		Position = position;
		WorldUp = up;
		Yaw = yaw;
		Pitch = pitch;
		updateCameraVectors();
	}

	// Constructor with scalar values
	Camera(float posX, float posY, float posZ, float upX, float upY, float upZ, float yaw, float pitch) : Front(QVector3D(0.0f, 0.0f, -1.0f)), MouseSensitivity(SENSITIVITY), Zoom(ZOOM)
	{
		Position = QVector3D(posX, posY, posZ);
		WorldUp = QVector3D(upX, upY, upZ);
		Yaw = yaw;
		Pitch = pitch;
		updateCameraVectors();
	}

	// Returns the view matrix calculated using Euler Angles and the LookAt Matrix
	QMatrix4x4 GetViewMatrix()
	{	
		QMatrix4x4 matrix;
		matrix.setToIdentity();
		matrix.lookAt(Position, Position + Front, Up);
		return matrix;
	}

	/**
	 * Set Eye-Position
	 * @param vector - Vector
	 */
	void setPosition(QVector3D vector) {
		Position = vector;
		updateCameraVectors();
	}

	/**
	 * Set Yaw-Angle in Degrees
	 * @param yaw Yaw-Angle
	 */
	void setYaw(float yaw) {
		Yaw = yaw;
		updateCameraVectors();
	}


	/**
	 * Set Pitch-Angle in Degrees
	 * @param pitch Pitch-Angle
	 */
	void setPitch(float pitch) {
		Pitch = pitch;
		updateCameraVectors();
	}

	// Process input received from any keyboard-like input system. 
	// Accepts input parameter in the form of camera defined ENUM
	// (to abstract it from windowing systems)
	void translate(Camera_Movement direction, float velocity)
	{
		if (direction == FORWARD)
			Position += Front * velocity;
		if (direction == BACKWARD)
			Position -= Front * velocity;
		if (direction == LEFT)
			Position -= Right * velocity;
		if (direction == RIGHT)
			Position += Right * velocity;
		if (direction == UP)
			Position += Up * velocity;
		if (direction == DOWN)
			Position -= Up * velocity;
	}

	// processes input received from a mouse input system.
	// Expects the offset value in both the x and y direction.
	void rotate(float xoffset, float yoffset, GLboolean constrainPitch = true)
	{

		/* TODO: ARC-BALL CAM */

		xoffset *= MouseSensitivity;
		yoffset *= MouseSensitivity;

		Yaw += xoffset;
		Pitch += yoffset;

		// Make sure that when pitch is out of bounds, screen doen't get flipped
		if (constrainPitch)
		{
			if (Pitch > 89.0f)
				Pitch = 89.0f;
			if (Pitch < -89.0f)
				Pitch = -89.0f;
		}

		//Update Front, Right and Up Vectors using the updated Eular angles
		updateCameraVectors();
	}

	void printParameters() {
		printf("Cam-Parameters:\n");
		printf(" Pos: %f, %f, %f\n", Position.x(), Position.y(), Position.z());
		printf(" Yaw: %f\n", Yaw);
		printf(" Pitch: %f\n", Pitch);
	};

private:
	// Calculates the front vector from the Camera's (updated) Euler Angles
	void updateCameraVectors()
	{
		// Calculate the new Front vector
		QVector3D front;
		front.setX(qCos(qDegreesToRadians(Yaw)) * qCos(qDegreesToRadians(Pitch)));
		front.setY(qSin(qDegreesToRadians(Pitch)));
		front.setZ(qSin(qDegreesToRadians(Yaw)) * qCos(qDegreesToRadians(Pitch)));
		front.normalize();
		Front = front;
		// Also re-calculate the Right and Up vector
		QVector3D right;
		right = right.crossProduct(Front, WorldUp);
		right.normalize();
		Right = right;  // Normalize the vectors, because their length gets closer to 0 the more you look up or down which results in slower movement.
		QVector3D up;
		up = up.crossProduct(Right, Front);
		up.normalize();
		Up = up;
	}
};

#endif // !CAMERA_H
