#include "mappingWidget.h"

/**
 * 0 - disabled
 * 1 - head
 * 2 - scull
 * 3 - cube Debug
 */
#define ENABLE_MODEL 1

MappingWidget::MappingWidget(QWidget *parent) :
QOpenGLWidget(parent)
{
    mBgColor = QColor::fromRgb(0, 0, 0);
}

/**
 * Initilisation of OpenGL
 */
void MappingWidget::initializeGL(){
	makeCurrent();
	/* Init OpenGL Functions */
	initializeOpenGLFunctions();

	/* OpenGL Operations*/
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glEnable(GL_ALPHA_TEST);
	glEnable(GL_CULL_FACE);

	/* Background Color */
	float r = ((float)mBgColor.darker().red()) / 255.0f;
	float g = ((float)mBgColor.darker().green()) / 255.0f;
	float b = ((float)mBgColor.darker().blue()) / 255.0f;

	glClearColor(r, g, b, 1.0f);

	/* Shader */
	mShader = new QOpenGLShaderProgram(this);
	mShader->addShaderFromSourceFile(QOpenGLShader::Vertex, "./Ressources/Shader/modelVertexShader.vs");
	mShader->addShaderFromSourceFile(QOpenGLShader::Fragment, "./Ressources/Shader/modelFragmentShader.fs");
	mShader->link();

	/* get Shader Location */
	mView = mShader->uniformLocation("view");
	mProjection = mShader->uniformLocation("projection");
	mIntensity = mShader->uniformLocation("intensity");
	mModel = mShader->uniformLocation("model");
	mViewPos = mShader->uniformLocation("viewPos");
	mObjectColor = mShader->uniformLocation("objectColor");
	
	/* Object */
	object = new Object3D(mShader);
	/* DEBUG set position */
	object->setPosition({ 0.0f, 0.0f, -1.0f });
	object->setScale({ 0.01f, 0.01f, 0.01f });

	/* Camera */
	cam = new Camera(QVector3D(-0.053f, 0.043f, 0.0f));

	/* Light */
	light = new PointLight();

	// load models
	// -----------
#if ENABLE_MODEL
	//Model * male = new Model("./Ressources/objects/body/male.obj");

	//Model * sceleton = new Model("./Ressources/objects/skeleton/group4.obj");
		//male->setScale({ 0.02f, 0.02f, 0.02f });

	//sceleton->setScale({ 0.08f, 0.08f, 0.08f });
#if ENABLE_MODEL == 1 || ENABLE_MODEL == 3
	// Head Model
	Model * head = new Model("./Ressources/objects/head/male_head_obj_centered.obj");
	head->setScale({ 0.006f,0.006f,0.006f });
	// CHANGE HERE OBJECT!!!
	model = head;
#elif ENABLE_MODEL == 2
	// Scull Model
	Model * scull = new Model("./Ressources/objects/head/head3.obj");
	scull->setScale({ 0.006f,0.006f,0.006f });
	// CHANGE HERE OBJECT!!!
	model = scull;

#endif
	
	model->setPosition({ 0.0f, 0.0f, -0.777f });
#endif // !ENABLE_MODEL
		
}

void MappingWidget::resizeGL(int width, int height)
{
    makeCurrent();
    glViewport(0, 0, (GLint)width, (GLint)height);
	mWidth = width;
	mHeight = height;

    glMatrixMode(GL_PROJECTION);						//Set to Projection Mode
    glLoadIdentity();

    glOrtho(0, width, -height, 0, 0, 1);

    glMatrixMode(GL_MODELVIEW);							// Matrix Mode to Model View

    recalculatePosition();

    emit imageSizeChanged(mRenderWidth, mRenderHeight);

    updateScene();
}

void MappingWidget::updateScene()
{
    if (this->isVisible()) update();
}

void MappingWidget::paintGL()
{
    makeCurrent();

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);					//Clear Color and depth buffer

	/* Cam Image Background */
	renderImage();
	/* 3D Scene */
	draw3DScene();
}

void MappingWidget::renderImage()
{
	drawMutex.lock();
    makeCurrent();
    glClear(GL_COLOR_BUFFER_BIT);			/// Clear Color Buffer 
    if (!mRenderQtImg.isNull())
    {
        glLoadIdentity();
        glPushMatrix();
        {
            if (mResizedImg.width() <= 0)
            {
                if (mRenderWidth == mRenderQtImg.width() && mRenderHeight == mRenderQtImg.height())
                    mResizedImg = mRenderQtImg;
                else
                    mResizedImg = mRenderQtImg.scaled(QSize(mRenderWidth, mRenderHeight),
                                                      Qt::IgnoreAspectRatio,
                                                      Qt::SmoothTransformation);
            }
            // ---> Centering image in draw area
            glRasterPos2i(mRenderPosX, mRenderPosY);
            glPixelZoom(1, -1);
            glDrawPixels(mResizedImg.width(), mResizedImg.height(), GL_RGBA, GL_UNSIGNED_BYTE, mResizedImg.bits());
        }
        glPopMatrix();
        // end
        glFlush();
    }
	drawMutex.unlock();
}

void MappingWidget::recalculatePosition()
{
    mImgRatio = (float)mOrigImage.cols/(float)mOrigImage.rows;

    mRenderWidth = this->size().width();
    mRenderHeight = floor(mRenderWidth / mImgRatio);

    if (mRenderHeight > this->size().height())
    {
        mRenderHeight = this->size().height();
        mRenderWidth = floor(mRenderHeight * mImgRatio);
    }

    mRenderPosX = floor((this->size().width() - mRenderWidth) / 2);
    mRenderPosY = -floor((this->size().height() - mRenderHeight) / 2);

    mResizedImg = QImage();
}

bool MappingWidget::showImage(const cv::Mat& image)
{
	drawMutex.lock();
    if (image.channels() == 3)
        cvtColor(image, mOrigImage, CV_BGR2RGBA);
    else if (image.channels() == 1)
        cvtColor(image, mOrigImage, CV_GRAY2RGBA);
	else if (image.channels() == 4)
		mOrigImage = image;
    else return false;

    mRenderQtImg = QImage((const unsigned char*)(mOrigImage.data),
                          mOrigImage.cols, mOrigImage.rows,
                          mOrigImage.step1(), QImage::Format_RGB32);
	
    recalculatePosition();

    updateScene();
	drawMutex.unlock();
    return true;
}

/**
 * Set 3D Szene Transparency
 * @param intensity - Intensity of 3D Model (0.0-1.0)
 */
void MappingWidget::setIntensity(int intensity)
{
	intensity3D = intensity / 100.0f;
}

/**
 * Draw 3D Szene 
 */
void MappingWidget::draw3DScene() {
	// configure global opengl state
	// -----------------------------
	glEnable(GL_DEPTH_TEST);		// Enable Depth Test for transparenz
	   
	// Set Camera 
	QMatrix4x4 view;
	view = cam->GetViewMatrix();
	

	//Projection 
	QMatrix4x4 projection;
	
	/* Scale 3D Projection to Image Size*/
	if (mResizedImg.width() > 0 && mResizedImg.height() > 0) {
		int llcX = 0;
		int llcY = 0;

		if (mResizedImg.width() < mWidth) {
			llcX = (mWidth -mResizedImg.width()) / 2;
		}
		else if (mResizedImg.height() < mHeight) {
			llcY = (mHeight - mResizedImg.height()) / 2;
		}
		
		glViewport(llcX, llcY, (GLint)mResizedImg.width(), (GLint)mResizedImg.height());

		float h = (float)mResizedImg.width() / 1920 * fovX;
		float v = (float)mResizedImg.height() / 1080 * fovY;
		projection.perspective(fovY,h/v, 0.1f, 5000.0f);

		//printf("%f :%f\n\n", fovY, h/v);
		//projection.perspective(80.0f, 1920 / 1080, 0.1f, 5000.0f);
		//printf("%i, %i\n", mResizedImg.width(), mResizedImg.height());
		//qDebug() << projection;
	}	
	
	mShader->bind();				// Activate Shader
	mShader->setUniformValue(mViewPos, cam->Position);
	mShader->setUniformValue(mView, view);
	mShader->setUniformValue(mProjection, projection);

	//Transparency of Object
	mShader->setUniformValue(mIntensity, intensity3D);
	//Set Color of Object (TODO: NOT NECESSARY IF TEXTURES EXIST)
	mShader->setUniformValue(mObjectColor, QVector3D(1.0, 1.0, 1.0));

	//Point Light
	light->Draw(mShader);

	//Draw MOdel
#if !DISABLE_MODEL

#if ENABLE_MODEL == 3
	object->draw(mShader);
#else
	model->Draw(mShader);
#endif
#endif //!DISABLE_MODEL
	
	//object->draw(mShader);
	glDisable(GL_DEPTH_TEST);

	/* DEBUG Rotation */
//	model->setRotation(QVector3D(model->getRotation().x(),
//		model->getRotation().y() + 1,
//		model->getRotation().z())); 

	mShader->release();			//Deativate Shader
}

void MappingWidget::setObjectPosition(float x, float y, float z)
{
	object->setPosition({ x, y, z });
	model->setPosition({ x, y, z});
}

void MappingWidget::setObjectRotation(cv::Point3f dirVec) {
	model->setRotation(
						QVector3D(dirVec.x,
							dirVec.y,
							-dirVec.z)
						);
}
 