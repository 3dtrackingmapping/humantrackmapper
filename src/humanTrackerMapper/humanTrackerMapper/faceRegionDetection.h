#ifndef _faceRegionDetection_H_
#define _faceRegionDetection_H_

#include <opencv2/opencv.hpp>
#include <opencv2\face.hpp>
#include <opencv2/tracking.hpp>
#include <opencv2/core/ocl.hpp>

#include "faceLandmarks.h"

using namespace std;
using namespace cv;

#define COLOR_RED Scalar(0, 0, 255)
#define COLOR_GREEN Scalar(0, 255, 0)
#define COLOR_BLUE Scalar(255, 0,0)
#define COLOR_LIGHT_BLUE Scalar(255, 200, 0)

/**
 * Class allows Detection and Tracking of a Face in an RGB Image.
 */
class FaceRegionDetection {
public:
	/** Defines Types for Tracking-Methods. */
	typedef enum {
		TRACKER_BOOSTING,
		TRACKER_MIL,
		TRACKER_KCF,
		TRACKER_TLD,
		TRACKER_MEDIANFLOW,
		TRAKER_GOTURN,
		TRACKER_MOUSSE,
		TRACKER_CSRT,
		TRACKER_NUM_TYPES
	} TrackerType;

	/**
	 * Constructor initializes Components.
	 */
	FaceRegionDetection();
	
	/**
	 * Detects a FaceRegion in an Color-Image.
	 * 
	 * @param inputFrame Original Image
	 * @param outputVisualization Output Image with visible Detections
	 * @param faceRegionAvailable defines wheter a faceRegion was found or not
	 * @param landmarks found landmarks on face. In best case 68 points.
	 * @return the detected Region of the Face (Rect can reach over the outside borders of the Frame!)
	 */
	cv::Rect detectFaceRegion(cv::Mat& inputFrame, cv::Mat& outputVisualization, bool& faceRegionAvailable, FaceLandmarks  * landmarks);

	/**
	 * Sets the current Tracking Method.
	 * 
	 * @param eTrackerType new Tracking Method
	 */
	void setTrackerType(TrackerType eTrackerType);

	/**
	 * Sets if detected facial landmarks should be shown on output Image.
	 * 
	 * @param status new Status
	 */
	void setShowLandmarks(bool status);

	/**
	* Sets if detected face region should be shown on output Image.
	*
	* @param status new Status
	*/
	void setShowDetectedFaceRegion(bool status);

	/**
	* Sets if detected face region should be shown on output Image.
	*
	* @param status new Status
	*/
	void setShowTrackedFaceRegion(bool status);

	/**
	 * Disables all Visualization features for better performance.
	 */
	void disableVisualization();

private:
	//Face Detector (Facial Landmark)
	/** Acceptable variance of face size in relation to average face area size. */
	const float acceptableVariance = 0.3f;
	/** Scaling-factor for detected face region. (multiplied value added on current size) */
	const float faceScaleFactor = 0.60f;
	/** Weighting for current detected area. */
	const float inputWeight = 50;
	/** Scaling-factor for inaccurate face regions. (multiplied value added on current size) */
	const float inacurrateIncreaseFactor = 0.5f;

	/* FaceDetector Instance. **/
	CascadeClassifier faceDetector;
	/** Facemark for facial landmark detection. */
	Ptr<face::Facemark> facemark;
	/** Average Face Region of previous calls. */
	float averageArea;
	/** Number of previous calls. */
	int detections;
	/** Current detected face region. */
	Rect2d detectedFaceRegion;
	/** Specifies if a face region was found. */
	bool hasFaceRegion;

	/** Initializes the Face Detector. */
	void initFaceDetector();
	/** 
	 * Detects a Face Region in an Image. 
	 * 
	 * @param inputFrame Original Image
	 * @param outputVisualization Output Image with visible Detections
	 * @param faceRegion the detected Region of the Face (Rect can reach over the outside borders of the Frame!)
	 * @param isAccurate face found in frame or previous found position used
	 * @param increaseWhenInaccurate if set, inaccurate faceregions will be scaled
	 * @param landmarks found landmarks on face. In best case 68 points.
	 * @return if faceregion is filled
	 */
	bool detectFaceRegion(cv::Mat& inputFrame, 
						  cv::Mat& outputVisualization, 
						  cv::Rect& faceRegion, 
						  bool& isAccurate,		
						  bool increaseWhenInaccurate, 
						  vector<vector<Point2f>>& landmarks);

	//Face Tracker
	/** Saves current Tracking Method. */
	TrackerType currentTracker;
	/** Saves if Initialization was performed. */
	bool isFaceTrackerInitialized;
	/** Tracker-Object. */
	Ptr<Tracker> tracker;
	/** Tracked face region. */
	Rect2d trackedFaceRegion;

	/** 
	 * Initializes the Face Tracker. 
	 *
	 * @param faceRegion Region to be tracked
	 * @param frame Image with face region 
	 */
	void initFaceTracker(Rect2d faceRegion, cv::Mat& frame);
	/**
	 * Returns String of a TrackerType.
	 * 
	 * @param eType TrackerType
	 * @return name of the type
	 */
	string trackerTypeToString(TrackerType eType);
	/**
	* Tracks a Face Region in an Image.
	*
	* @param inputFrame Original Image
	* @param outputVisualization Output Image with visible Detections
	* @return if face was tracked successful
	*/
	bool trackFaceRegion(cv::Mat& inputFrame, cv::Mat& outputVisualization);

	//Visualization
	/** Defines if detetcted Facial Landmarks should be drawn. */
	bool showLandmarks;
	/** Defines if the detected Face region should be drawn. */
	bool showDetectedFaceRegion;
	/** Defines if the tracked Face region should be drawn. */
	bool showTrackedFaceRegion;

	/**
	 * src: https://www.learnopencv.com/head-pose-estimation-using-opencv-and-dlib/
	 */
	void drawPolyline(Mat &im, const vector<Point2f> &landmarks, const int start, const int end, bool isClosed = false);
	/**
	 * src: https://www.learnopencv.com/head-pose-estimation-using-opencv-and-dlib/
	 */
	void drawLandmarks(Mat &im, vector<Point2f> &landmarks);

	/**
	 * Checks if Rect is inside the Image.
	 * 
	 * @param im Image
	 * @param rect Rect
	 * @return true if rect is inside image
	 */
	bool isRectInsideImage(const Mat &im, const Rect2d& rect);
};

#endif // _faceRegionDetection_H_