#ifndef HUMANTRACKERMAPPER_H
#define HUMANTRACKERMAPPER_H

// DEBUG Kinect output to OpenCV windows
#define KINECT_OUPUT_DEBUG 0
#define USE_KINECT_EMU 0

// Calls per Second for Camera Timer
#define FRAMES_PER_SECOND 30

//System Includes 
#include <string>
#include <iostream>
#include <stdio.h>
#include <algorithm>

//QT Includes 
#include <QtWidgets/QMainWindow>
#include <QMainWindow>
#include <QCloseEvent>
#include <QMessageBox>
#include <QInputDialog>
#include <QKeyEvent>

//OpenCV Includes 
#include <opencv\highgui.h>
#include <opencv\cv.h>
#include <opencv2\opencv.hpp>
#include <opencv2/imgproc/imgproc.hpp>

// ImageProcessing
#include "imageProcessing.h" 

//Other Includes 
#include "ui_humanTrackerMapper.h"
#include "kinect.h"
#include "kinect_emulator.h"
#include "captureInterface.h"
#include "faceRegionDetection.h"
#include "faceLandmarks.h"

using namespace openni;

namespace Ui {
	class HumanTrackerMapper;
}

/**
 * Class controlls mainwindow ui.
 */
class HumanTrackerMapper : public QMainWindow
{
	Q_OBJECT

public:
	/** Constructor */
	explicit HumanTrackerMapper(QWidget *parent = 0);
	/** Destructor */
	~HumanTrackerMapper();

private:
	// ----------- Private Members

	/** mainWindow reference */
	Ui::HumanTrackerMapper *ui;

	/** Kinect instance for camera interactions. */
#if	USE_KINECT_EMU
	Kinect_EMU* kinect = NULL;
#else
	Kinect* kinect = NULL;
#endif // USE_KINECT_EMU

	/** Face detector */
	FaceRegionDetection faceRegionDetector;
	/** Face marks */
	FaceLandmarks * facemarks;


	/** Timer ID for frame-update-timer. */
	int timerIdentifier;

	// ----------- Private Functions 

	/**
	 * Timer function is called on every Frame.
	 */
	void timerEvent(QTimerEvent*);
	
	/**
	 * Initialization.
	 */
	void initialization();
	
	/**
	 * UserInput - Keyboard Event Handler.
	 * @param event Keyboard Event
	 */
	void keyPressEvent(QKeyEvent * event);

private slots:
	/**
	 * About Info is shown.
	 */
	void on_action_About_triggered();
	/**
	 * Initializes camera and Openni.
	 */
	void on_action_Initialize_triggered();
	/**
	 * Saves the current images from camera.
	 */
	void on_action_SaveImages_clicked();
	/**
	 * Changes an current camera.
	 */
	void changeCam();
	/**
	 * Is called on program exit.
	 */
	void closeEvent(QCloseEvent *event);
	/**
	 * Updates the available Devices in GUI.
	 */
	void update_Devices();
};

#endif // HUMANTRACKERMAPPER_H
