#version 330 core
layout(location = 0) in vec3 aPos;
layout(location = 1) in vec3 aNormal;
layout(location = 2) in vec2 aTexCoords;
layout(location = 3) in vec3 aTangent;
layout(location = 4) in vec3 aBitangent;
layout(location = 5) in vec3 aCol;

out vec4 vertexColor;
out vec2 TexCoords;

out vec3 Normal;
out vec3 FragPos;

uniform highp mat4 model;
uniform highp mat4 view;
uniform highp mat4 projection;

void main() {
	FragPos = vec3(model * vec4(aPos, 1.0));		// Vertex Position multiplied by the model Matrix
	Normal = mat3(transpose(inverse(model)))*aNormal; //Normal Multiplied by the transposed inverse Model Matrix

	TexCoords = aTexCoords; 
	gl_Position = projection * view * vec4(FragPos, 1.0);
}