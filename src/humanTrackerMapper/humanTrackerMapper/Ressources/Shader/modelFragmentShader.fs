#version 330 core 

struct PointLight {
    vec3 position;
    
    float constant;
    float linear;
    float quadratic;
	
    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
};

in vec4 vertexColor;
in vec2 TexCoords;
in vec3 Normal;
in vec3 FragPos;

uniform sampler2D texture_diffuse1;
uniform highp float intensity;

uniform highp vec3 viewPos;
uniform highp vec3 objectColor;

uniform highp PointLight pointLight;


// function prototypes
vec3 CalcPointLight(PointLight light, vec3 normal, vec3 fragPos, vec3 viewDir);

/**
 * TODO: weitere Lichtquellen Phong Beleuchtung und Lichtquellen
 * TODO: weitere Textur-Typen
 */

void main() {
	// properties
	vec3 norm = normalize(Normal);
	vec3 viewDir = normalize(viewPos - FragPos);

	// directional Lightning
	vec3 result = CalcPointLight(pointLight, norm, FragPos, viewDir);

	//output
	gl_FragColor = vec4(result, intensity);
	
   //gl_FragColor = vec4(Normal,intensity);
   //gl_FragColor = vec4(1.0);
   //gl_FragColor = vertexColor;
   //gl_FragColor = vec4(texture(texture_diffuse1, TexCoords).rgb, intensity);
}

vec3 CalcPointLight(PointLight light, vec3 normal, vec3 fragPos, vec3 viewDir) {
	vec3 lightDir = normalize(light.position - fragPos);
	// diffuse shading
	float diff = max(dot(normal,lightDir),0.0);
	// specular shading
	vec3 reflectDir = reflect(-lightDir, normal);
	float spec = pow(max(dot(viewDir,reflectDir),0.0),32); // add (material) shininess

	vec3 ambient = light.ambient * objectColor;
	vec3 diffuse = light.diffuse * diff * objectColor;
	vec3 specular = light.specular * spec * objectColor;
	
	return (ambient + diffuse + specular);
}