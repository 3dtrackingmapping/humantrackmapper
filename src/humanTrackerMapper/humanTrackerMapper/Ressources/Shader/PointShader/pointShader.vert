#version 330 core
layout(location = 0) in vec3 aPos;
layout(location = 5) in vec3 aCol;

out vec4 vertexColor;

uniform highp mat4 view;
uniform highp mat4 projection;

void main() {
	vec3 FragPos = aPos;		// Vertex Position multiplied by the model Matrix
	vertexColor = vec4(aCol, 1.0);
	gl_PointSize = 2.0f;
	gl_Position = projection * view * vec4(FragPos, 1.0);
}