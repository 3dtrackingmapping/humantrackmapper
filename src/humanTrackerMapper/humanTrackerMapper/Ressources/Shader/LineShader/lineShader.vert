#version 330 core
layout(location = 0) in vec3 aPos;
layout(location = 5) in vec3 aCol;

out vec4 vertexColor;

uniform highp mat4 view;
uniform highp mat4 model;
uniform highp mat4 projection;

void main() {

	vec3 FragPos = aPos;
	vertexColor = vec4(aCol, 1.0);
	if(aCol == vec3(0.0)) {
		vertexColor = vec4(1.0);
	}
	gl_Position = projection * view * model * vec4(FragPos, 1.0);
}