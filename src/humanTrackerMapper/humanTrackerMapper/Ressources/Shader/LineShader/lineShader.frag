#version 330 core 
out vec4 gl_FragColor;

in vec4 vertexColor;

void main() {
	gl_FragColor = vertexColor;
}