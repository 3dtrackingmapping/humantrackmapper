/********************************************************************************
** Form generated from reading UI file 'humanTrackerMapper.ui'
**
** Created by: Qt User Interface Compiler version 5.11.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_HUMANTRACKERMAPPER_H
#define UI_HUMANTRACKERMAPPER_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QSlider>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>
#include <mappingWidget.h>
#include <pointCloudWidget.h>

QT_BEGIN_NAMESPACE

class Ui_HumanTrackerMapper
{
public:
    QAction *action_About;
    QAction *action_Exit;
    QAction *action_Initialize;
    QWidget *centralWidget;
    QGridLayout *gridLayout;
    QTabWidget *tabWidget;
    QWidget *mapping;
    QGridLayout *gridLayout_4;
    QGridLayout *mappigLayout;
    QHBoxLayout *horizontalLayout_3;
    MappingWidget *mappingWidget;
    QVBoxLayout *rightSide;
    QHBoxLayout *horizontalLayout_7;
    QSpacerItem *horizontalSpacer_4;
    QLabel *alphaLabel;
    QSpacerItem *horizontalSpacer_3;
    QHBoxLayout *horizontalLayout_6;
    QSpacerItem *horizontalSpacer_6;
    QSlider *alphaSlider;
    QSpacerItem *horizontalSpacer_5;
    QHBoxLayout *horizontalLayout_4;
    QSpacerItem *horizontalSpacer_7;
    QSpinBox *alphaValue;
    QSpacerItem *horizontalSpacer_8;
    QHBoxLayout *horizontalLayout_10;
    QSpacerItem *verticalSpacer_2;
    QHBoxLayout *horizontalLayout_11;
    QSpacerItem *horizontalSpacer_15;
    QCheckBox *activateRotation;
    QSpacerItem *horizontalSpacer_16;
    QHBoxLayout *horizontalLayout_5;
    QSpacerItem *horizontalSpacer_10;
    QPushButton *action_SaveImages;
    QSpacerItem *horizontalSpacer_9;
    QWidget *pointCloud;
    QGridLayout *gridLayout_5;
    QGridLayout *pointCloudLayout;
    QHBoxLayout *rightSide_2;
    PointCloudWidget *pointCloudWidget;
    QVBoxLayout *verticalLayout_3;
    QHBoxLayout *horizontalLayout;
    QSpacerItem *horizontalSpacer_2;
    QLabel *label;
    QSpacerItem *horizontalSpacer;
    QVBoxLayout *verticalLayout_2;
    QHBoxLayout *horizontalLayout_2;
    QSpacerItem *horizontalSpacer_12;
    QVBoxLayout *verticalLayout_4;
    QCheckBox *showPC;
    QVBoxLayout *verticalLayout_5;
    QHBoxLayout *horizontalLayout_8;
    QSpacerItem *horizontalSpacer_13;
    QRadioButton *showBtnFull;
    QHBoxLayout *horizontalLayout_9;
    QSpacerItem *horizontalSpacer_14;
    QRadioButton *showBtnExtr;
    QCheckBox *showModel;
    QCheckBox *showVecs;
    QCheckBox *showCam;
    QSpacerItem *horizontalSpacer_11;
    QSpacerItem *verticalSpacer;
    QVBoxLayout *verticalLayout;
    QMenuBar *menuBar;
    QMenu *menu_File;
    QMenu *menu_Help;
    QMenu *menu_Camera;
    QMenu *menuSettings;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *HumanTrackerMapper)
    {
        if (HumanTrackerMapper->objectName().isEmpty())
            HumanTrackerMapper->setObjectName(QStringLiteral("HumanTrackerMapper"));
        HumanTrackerMapper->resize(1064, 759);
        HumanTrackerMapper->setStyleSheet(QStringLiteral("background:rgb(40,40,40); color:rgb(200,200,200)"));
        action_About = new QAction(HumanTrackerMapper);
        action_About->setObjectName(QStringLiteral("action_About"));
        action_Exit = new QAction(HumanTrackerMapper);
        action_Exit->setObjectName(QStringLiteral("action_Exit"));
        action_Initialize = new QAction(HumanTrackerMapper);
        action_Initialize->setObjectName(QStringLiteral("action_Initialize"));
        action_Initialize->setEnabled(false);
        centralWidget = new QWidget(HumanTrackerMapper);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        gridLayout = new QGridLayout(centralWidget);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        tabWidget = new QTabWidget(centralWidget);
        tabWidget->setObjectName(QStringLiteral("tabWidget"));
        tabWidget->setStyleSheet(QLatin1String("QTabWidget::pane { /* The tab widget frame */\n"
"border-top: 2px solid rgb(80,80,80);\n"
"}\n"
"QTabWidget::tab-bar {\n"
"left: 5px; /* move to the right by 5px */\n"
"}\n"
"/* Style the tab using the tab sub-control. Note that it reads QTabBar _not_ QTabWidget */\n"
"QTabBar::tab {\n"
"/*background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #E1E1E1, stop: 0.4 #DDDDDD, stop: 0.5 #D8D8D8, stop: 1.0 #D3D3D3);*/\n"
"background:rgb(80,80,80);\n"
"border: 2px solid rgb(90,90,90);\n"
"border-bottom-color: rgb(90,90,90); /* same as the pane color */\n"
"border-top-left-radius: 4px;\n"
"border-top-right-radius: 4px;\n"
"min-width: 8ex;\n"
"padding: 2px;\n"
"}\n"
"QTabBar::tab:selected, QTabBar::tab:hover {\n"
"/*background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #fafafa, stop: 0.4 #f4f4f4, stop: 0.5 #e7e7e7, stop: 1.0 #fafafa);*/\n"
"background:rgb(100,100,100);\n"
"}\n"
"QTabBar::tab:selected {\n"
"border: 2px solid rgb(120,120,120);\n"
"border-bottom-color: rgb(100,100,100); /* same as pane c"
                        "olor */\n"
"}\n"
"QTabBar::tab:!selected {\n"
"margin-top: 2px; /* make non-selected tabs look smaller */\n"
"}"));
        mapping = new QWidget();
        mapping->setObjectName(QStringLiteral("mapping"));
        gridLayout_4 = new QGridLayout(mapping);
        gridLayout_4->setSpacing(6);
        gridLayout_4->setContentsMargins(11, 11, 11, 11);
        gridLayout_4->setObjectName(QStringLiteral("gridLayout_4"));
        mappigLayout = new QGridLayout();
        mappigLayout->setSpacing(6);
        mappigLayout->setObjectName(QStringLiteral("mappigLayout"));
        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setSpacing(6);
        horizontalLayout_3->setObjectName(QStringLiteral("horizontalLayout_3"));
        mappingWidget = new MappingWidget(mapping);
        mappingWidget->setObjectName(QStringLiteral("mappingWidget"));
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(mappingWidget->sizePolicy().hasHeightForWidth());
        mappingWidget->setSizePolicy(sizePolicy);
        mappingWidget->setMinimumSize(QSize(50, 50));

        horizontalLayout_3->addWidget(mappingWidget);

        rightSide = new QVBoxLayout();
        rightSide->setSpacing(0);
        rightSide->setObjectName(QStringLiteral("rightSide"));
        rightSide->setSizeConstraint(QLayout::SetMaximumSize);
        horizontalLayout_7 = new QHBoxLayout();
        horizontalLayout_7->setSpacing(0);
        horizontalLayout_7->setObjectName(QStringLiteral("horizontalLayout_7"));
        horizontalLayout_7->setSizeConstraint(QLayout::SetMaximumSize);
        horizontalSpacer_4 = new QSpacerItem(40, 20, QSizePolicy::Minimum, QSizePolicy::Minimum);

        horizontalLayout_7->addItem(horizontalSpacer_4);

        alphaLabel = new QLabel(mapping);
        alphaLabel->setObjectName(QStringLiteral("alphaLabel"));

        horizontalLayout_7->addWidget(alphaLabel);

        horizontalSpacer_3 = new QSpacerItem(40, 20, QSizePolicy::Minimum, QSizePolicy::Minimum);

        horizontalLayout_7->addItem(horizontalSpacer_3);


        rightSide->addLayout(horizontalLayout_7);

        horizontalLayout_6 = new QHBoxLayout();
        horizontalLayout_6->setSpacing(0);
        horizontalLayout_6->setObjectName(QStringLiteral("horizontalLayout_6"));
        horizontalLayout_6->setSizeConstraint(QLayout::SetMaximumSize);
        horizontalSpacer_6 = new QSpacerItem(40, 20, QSizePolicy::Maximum, QSizePolicy::Minimum);

        horizontalLayout_6->addItem(horizontalSpacer_6);

        alphaSlider = new QSlider(mapping);
        alphaSlider->setObjectName(QStringLiteral("alphaSlider"));
        QSizePolicy sizePolicy1(QSizePolicy::Maximum, QSizePolicy::Expanding);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(alphaSlider->sizePolicy().hasHeightForWidth());
        alphaSlider->setSizePolicy(sizePolicy1);
        alphaSlider->setToolTipDuration(1);
        alphaSlider->setMaximum(100);
        alphaSlider->setSliderPosition(100);
        alphaSlider->setOrientation(Qt::Vertical);

        horizontalLayout_6->addWidget(alphaSlider);

        horizontalSpacer_5 = new QSpacerItem(40, 20, QSizePolicy::Maximum, QSizePolicy::Minimum);

        horizontalLayout_6->addItem(horizontalSpacer_5);


        rightSide->addLayout(horizontalLayout_6);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setSpacing(0);
        horizontalLayout_4->setObjectName(QStringLiteral("horizontalLayout_4"));
        horizontalLayout_4->setSizeConstraint(QLayout::SetMaximumSize);
        horizontalSpacer_7 = new QSpacerItem(40, 20, QSizePolicy::Maximum, QSizePolicy::Minimum);

        horizontalLayout_4->addItem(horizontalSpacer_7);

        alphaValue = new QSpinBox(mapping);
        alphaValue->setObjectName(QStringLiteral("alphaValue"));
        alphaValue->setMaximum(100);
        alphaValue->setValue(100);

        horizontalLayout_4->addWidget(alphaValue);

        horizontalSpacer_8 = new QSpacerItem(40, 20, QSizePolicy::Maximum, QSizePolicy::Minimum);

        horizontalLayout_4->addItem(horizontalSpacer_8);


        rightSide->addLayout(horizontalLayout_4);

        horizontalLayout_10 = new QHBoxLayout();
        horizontalLayout_10->setSpacing(6);
        horizontalLayout_10->setObjectName(QStringLiteral("horizontalLayout_10"));
        horizontalLayout_10->setContentsMargins(-1, 0, -1, -1);
        verticalSpacer_2 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        horizontalLayout_10->addItem(verticalSpacer_2);


        rightSide->addLayout(horizontalLayout_10);

        horizontalLayout_11 = new QHBoxLayout();
        horizontalLayout_11->setSpacing(0);
        horizontalLayout_11->setObjectName(QStringLiteral("horizontalLayout_11"));
        horizontalLayout_11->setContentsMargins(-1, -1, -1, 0);
        horizontalSpacer_15 = new QSpacerItem(10, 20, QSizePolicy::Minimum, QSizePolicy::Minimum);

        horizontalLayout_11->addItem(horizontalSpacer_15);

        activateRotation = new QCheckBox(mapping);
        activateRotation->setObjectName(QStringLiteral("activateRotation"));

        horizontalLayout_11->addWidget(activateRotation);

        horizontalSpacer_16 = new QSpacerItem(10, 20, QSizePolicy::Minimum, QSizePolicy::Minimum);

        horizontalLayout_11->addItem(horizontalSpacer_16);


        rightSide->addLayout(horizontalLayout_11);

        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setSpacing(0);
        horizontalLayout_5->setObjectName(QStringLiteral("horizontalLayout_5"));
        horizontalLayout_5->setSizeConstraint(QLayout::SetMaximumSize);
        horizontalSpacer_10 = new QSpacerItem(40, 20, QSizePolicy::Maximum, QSizePolicy::Minimum);

        horizontalLayout_5->addItem(horizontalSpacer_10);

        action_SaveImages = new QPushButton(mapping);
        action_SaveImages->setObjectName(QStringLiteral("action_SaveImages"));

        horizontalLayout_5->addWidget(action_SaveImages);

        horizontalSpacer_9 = new QSpacerItem(40, 20, QSizePolicy::Maximum, QSizePolicy::Minimum);

        horizontalLayout_5->addItem(horizontalSpacer_9);


        rightSide->addLayout(horizontalLayout_5);


        horizontalLayout_3->addLayout(rightSide);


        mappigLayout->addLayout(horizontalLayout_3, 1, 0, 1, 1);


        gridLayout_4->addLayout(mappigLayout, 0, 1, 1, 1);

        tabWidget->addTab(mapping, QString());
        pointCloud = new QWidget();
        pointCloud->setObjectName(QStringLiteral("pointCloud"));
        gridLayout_5 = new QGridLayout(pointCloud);
        gridLayout_5->setSpacing(6);
        gridLayout_5->setContentsMargins(11, 11, 11, 11);
        gridLayout_5->setObjectName(QStringLiteral("gridLayout_5"));
        pointCloudLayout = new QGridLayout();
        pointCloudLayout->setSpacing(0);
        pointCloudLayout->setObjectName(QStringLiteral("pointCloudLayout"));
        rightSide_2 = new QHBoxLayout();
        rightSide_2->setSpacing(0);
        rightSide_2->setObjectName(QStringLiteral("rightSide_2"));
        rightSide_2->setSizeConstraint(QLayout::SetMinimumSize);
        pointCloudWidget = new PointCloudWidget(pointCloud);
        pointCloudWidget->setObjectName(QStringLiteral("pointCloudWidget"));
        sizePolicy.setHeightForWidth(pointCloudWidget->sizePolicy().hasHeightForWidth());
        pointCloudWidget->setSizePolicy(sizePolicy);
        pointCloudWidget->setMinimumSize(QSize(0, 0));

        rightSide_2->addWidget(pointCloudWidget);

        verticalLayout_3 = new QVBoxLayout();
        verticalLayout_3->setSpacing(6);
        verticalLayout_3->setObjectName(QStringLiteral("verticalLayout_3"));
        verticalLayout_3->setSizeConstraint(QLayout::SetMaximumSize);
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(6);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        horizontalLayout->setSizeConstraint(QLayout::SetMaximumSize);
        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Maximum, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_2);

        label = new QLabel(pointCloud);
        label->setObjectName(QStringLiteral("label"));

        horizontalLayout->addWidget(label);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Maximum, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);


        verticalLayout_3->addLayout(horizontalLayout);

        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setSpacing(0);
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        horizontalSpacer_12 = new QSpacerItem(5, 20, QSizePolicy::Minimum, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_12);

        verticalLayout_4 = new QVBoxLayout();
        verticalLayout_4->setSpacing(4);
        verticalLayout_4->setObjectName(QStringLiteral("verticalLayout_4"));
        showPC = new QCheckBox(pointCloud);
        showPC->setObjectName(QStringLiteral("showPC"));
        showPC->setChecked(true);

        verticalLayout_4->addWidget(showPC);

        verticalLayout_5 = new QVBoxLayout();
        verticalLayout_5->setSpacing(0);
        verticalLayout_5->setObjectName(QStringLiteral("verticalLayout_5"));
        horizontalLayout_8 = new QHBoxLayout();
        horizontalLayout_8->setSpacing(0);
        horizontalLayout_8->setObjectName(QStringLiteral("horizontalLayout_8"));
        horizontalSpacer_13 = new QSpacerItem(15, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout_8->addItem(horizontalSpacer_13);

        showBtnFull = new QRadioButton(pointCloud);
        showBtnFull->setObjectName(QStringLiteral("showBtnFull"));
        showBtnFull->setChecked(false);

        horizontalLayout_8->addWidget(showBtnFull);


        verticalLayout_5->addLayout(horizontalLayout_8);

        horizontalLayout_9 = new QHBoxLayout();
        horizontalLayout_9->setSpacing(0);
        horizontalLayout_9->setObjectName(QStringLiteral("horizontalLayout_9"));
        horizontalSpacer_14 = new QSpacerItem(15, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout_9->addItem(horizontalSpacer_14);

        showBtnExtr = new QRadioButton(pointCloud);
        showBtnExtr->setObjectName(QStringLiteral("showBtnExtr"));
        showBtnExtr->setChecked(true);

        horizontalLayout_9->addWidget(showBtnExtr);


        verticalLayout_5->addLayout(horizontalLayout_9);


        verticalLayout_4->addLayout(verticalLayout_5);

        showModel = new QCheckBox(pointCloud);
        showModel->setObjectName(QStringLiteral("showModel"));

        verticalLayout_4->addWidget(showModel);

        showVecs = new QCheckBox(pointCloud);
        showVecs->setObjectName(QStringLiteral("showVecs"));
        showVecs->setChecked(true);

        verticalLayout_4->addWidget(showVecs);

        showCam = new QCheckBox(pointCloud);
        showCam->setObjectName(QStringLiteral("showCam"));

        verticalLayout_4->addWidget(showCam);


        horizontalLayout_2->addLayout(verticalLayout_4);

        horizontalSpacer_11 = new QSpacerItem(5, 20, QSizePolicy::Minimum, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_11);


        verticalLayout_2->addLayout(horizontalLayout_2);


        verticalLayout_3->addLayout(verticalLayout_2);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_3->addItem(verticalSpacer);


        rightSide_2->addLayout(verticalLayout_3);


        pointCloudLayout->addLayout(rightSide_2, 1, 0, 1, 1);

        verticalLayout = new QVBoxLayout();
        verticalLayout->setSpacing(0);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        verticalLayout->setSizeConstraint(QLayout::SetMinimumSize);

        pointCloudLayout->addLayout(verticalLayout, 0, 0, 1, 1);


        gridLayout_5->addLayout(pointCloudLayout, 0, 0, 1, 1);

        tabWidget->addTab(pointCloud, QString());

        gridLayout->addWidget(tabWidget, 0, 0, 1, 1);

        HumanTrackerMapper->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(HumanTrackerMapper);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 1064, 21));
        menuBar->setStyleSheet(QLatin1String("QMenuBar::item {\n"
"   background-color:rgb(40,40,40);\n"
"}\n"
"\n"
"QMenuBar::item:selected {\n"
"    background-color:rgb(80,80,80);\n"
"}\n"
"\n"
"QMenu {\n"
"    background-color:rgb(40,40,40);\n"
"    border-top: none;\n"
"    border-left:none;\n"
"    border-right:none;\n"
"    border-bottom:4px solid  rgb(150,150,150);\n"
"}\n"
"\n"
"QMenu::item {\n"
"    spacing: 3px; /* spacing between menu bar items */\n"
"    padding: 10px 85px 10px 20px;\n"
"    background: transparent;\n"
"}\n"
"\n"
"QMenu::item:selected {\n"
"    background-color: rgb(80,80,80);\n"
"    border-top: none;\n"
"    border-left:none;\n"
"    border-bottom:none;\n"
"    border-left:3px solid  rgb(150,150,150);\n"
"}"));
        menu_File = new QMenu(menuBar);
        menu_File->setObjectName(QStringLiteral("menu_File"));
        menu_Help = new QMenu(menuBar);
        menu_Help->setObjectName(QStringLiteral("menu_Help"));
        menu_Camera = new QMenu(menuBar);
        menu_Camera->setObjectName(QStringLiteral("menu_Camera"));
        menuSettings = new QMenu(menuBar);
        menuSettings->setObjectName(QStringLiteral("menuSettings"));
        HumanTrackerMapper->setMenuBar(menuBar);
        statusBar = new QStatusBar(HumanTrackerMapper);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        HumanTrackerMapper->setStatusBar(statusBar);

        menuBar->addAction(menu_File->menuAction());
        menuBar->addAction(menu_Camera->menuAction());
        menuBar->addAction(menuSettings->menuAction());
        menuBar->addAction(menu_Help->menuAction());
        menu_File->addAction(action_Initialize);
        menu_File->addSeparator();
        menu_File->addAction(action_Exit);
        menu_Help->addAction(action_About);

        retranslateUi(HumanTrackerMapper);
        QObject::connect(action_Exit, SIGNAL(triggered()), HumanTrackerMapper, SLOT(close()));
        QObject::connect(alphaSlider, SIGNAL(valueChanged(int)), alphaValue, SLOT(setValue(int)));
        QObject::connect(alphaValue, SIGNAL(valueChanged(int)), alphaSlider, SLOT(setValue(int)));
        QObject::connect(alphaValue, SIGNAL(valueChanged(int)), mappingWidget, SLOT(setIntensity(int)));

        tabWidget->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(HumanTrackerMapper);
    } // setupUi

    void retranslateUi(QMainWindow *HumanTrackerMapper)
    {
        HumanTrackerMapper->setWindowTitle(QApplication::translate("HumanTrackerMapper", "HumanTrackerMapper", nullptr));
        action_About->setText(QApplication::translate("HumanTrackerMapper", "&About", nullptr));
        action_Exit->setText(QApplication::translate("HumanTrackerMapper", "&Exit", nullptr));
        action_Initialize->setText(QApplication::translate("HumanTrackerMapper", "Initialize", nullptr));
        alphaLabel->setText(QApplication::translate("HumanTrackerMapper", "Alpha Value", nullptr));
#ifndef QT_NO_TOOLTIP
        alphaSlider->setToolTip(QString());
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_STATUSTIP
        alphaSlider->setStatusTip(QString());
#endif // QT_NO_STATUSTIP
#ifndef QT_NO_WHATSTHIS
        alphaSlider->setWhatsThis(QString());
#endif // QT_NO_WHATSTHIS
        activateRotation->setText(QApplication::translate("HumanTrackerMapper", "Model Rotation", nullptr));
        action_SaveImages->setText(QApplication::translate("HumanTrackerMapper", "Save Images", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(mapping), QApplication::translate("HumanTrackerMapper", "Mapping", nullptr));
        label->setText(QApplication::translate("HumanTrackerMapper", "Settings", nullptr));
        showPC->setText(QApplication::translate("HumanTrackerMapper", "Show Point Cloud", nullptr));
        showBtnFull->setText(QApplication::translate("HumanTrackerMapper", "Full", nullptr));
        showBtnExtr->setText(QApplication::translate("HumanTrackerMapper", "Extract", nullptr));
        showModel->setText(QApplication::translate("HumanTrackerMapper", "Show Model", nullptr));
        showVecs->setText(QApplication::translate("HumanTrackerMapper", "Show Vectors", nullptr));
        showCam->setText(QApplication::translate("HumanTrackerMapper", "Show Camera", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(pointCloud), QApplication::translate("HumanTrackerMapper", "Point Cloud", nullptr));
        menu_File->setTitle(QApplication::translate("HumanTrackerMapper", "&File", nullptr));
        menu_Help->setTitle(QApplication::translate("HumanTrackerMapper", "&Help", nullptr));
        menu_Camera->setTitle(QApplication::translate("HumanTrackerMapper", "Cameras", nullptr));
        menuSettings->setTitle(QApplication::translate("HumanTrackerMapper", "Settings", nullptr));
    } // retranslateUi

};

namespace Ui {
    class HumanTrackerMapper: public Ui_HumanTrackerMapper {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_HUMANTRACKERMAPPER_H
