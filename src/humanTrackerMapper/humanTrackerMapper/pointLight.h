#ifndef LIGHT_H
#define LIGHT_H

#include <QOpenGLShaderProgram>
#include <vector>
#include <qvector3d.h>
#include <qvector4d.h>
#include <qmatrix4x4.h>
#include <QMath.h>


class PointLight
{
public:
	// Light Attributes
	QVector3D	Position;
	QVector3D	Ambient;
	QVector3D	Diffuse;
	QVector3D	Specular;
	float		Constant;
	float		Linear;
	float		Quadratic;

	// Constructor with vectors
	PointLight(QVector3D ambient = QVector3D(0.1,0.1,0.1), QVector3D diffuse = QVector3D(1.0, 1.0, 1.0), QVector3D specular = QVector3D(0.5, 0.5, 0.5), QVector3D position = QVector3D(0.0, 0.0, 0.0))
	{
		Position = position;
		Ambient = ambient;
		Diffuse = diffuse;
		Specular = specular;
		Constant = 1.0f;
		Linear = 1.0f;
		Quadratic = 1.0f;
	}

	/**
	* Set Light Position
	* @param vector - Vector
	*/
	void setPosition(QVector3D vector) {
		Position = vector;
	}

	/**
	* Set Light Color
	* @param vector - Vector
	*/
	void setColor(QVector3D vector) {
		Ambient = vector;
		Diffuse = vector;
		Specular = vector;
	}

	//draws the model, and thus all its meshes
	void Draw(QOpenGLShaderProgram *shader)
	{
		shader->bind();
			shader->setUniformValue(shader->uniformLocation("pointLight.position"), Position);
			shader->setUniformValue(shader->uniformLocation("pointLight.ambient"), Ambient);
			shader->setUniformValue(shader->uniformLocation("pointLight.diffuse"), Diffuse);
			shader->setUniformValue(shader->uniformLocation("pointLight.specular"), Specular);
			shader->setUniformValue(shader->uniformLocation("pointLight.constant"), Constant);
			shader->setUniformValue(shader->uniformLocation("pointLight.linear"), Linear);
			shader->setUniformValue(shader->uniformLocation("pointLight.quadratic"), Quadratic);
		shader->release();
	}

private:

};

#endif // !LIGHT_H
