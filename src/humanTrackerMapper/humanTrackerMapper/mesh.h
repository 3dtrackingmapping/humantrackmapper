#ifndef MESH_H
#define MESH_H

#include <qvector3d.h>
#include <QOpenGLFunctions>
#include <QOpenGLVertexArrayObject>
#include <QOpenGLBuffer>
#include <QOpenGLShader>
#include <QOpenGLShaderProgram>
#include <QOpenGLTexture>
#include <QOpenGLTexture>

#include <string>
#include <fstream>
#include <sstream>
#include <iostream>
#include <vector>

using namespace std;

struct Vertex {
	/* position */
	QVector3D Position;
	/* normal */
	QVector3D Normal;
	/* texCoords */
	QVector2D TexCoords;
	/* tangent */
	QVector3D Tangent;
	/* bitangent */
	QVector3D Bitangent;
};

struct Texture {
	//unsigned int id;
	string type; // e.g diffuse / specular ...
	string path; //path to texture to compare with other textures
	QOpenGLTexture *texture;
};

class Mesh : protected QOpenGLFunctions {
public:
	/* Mesh Data */
	vector<Vertex> vertices;
	vector<unsigned int> indices;
	vector<Texture> textures;
	/* Buffer Objects */
	QOpenGLVertexArrayObject *VAO;
	bool print = false;

	/* Function */
	// constructor 
	Mesh(vector<Vertex> vertices,
		vector<unsigned int> indices,
		vector<Texture> textures)
	{
		this->vertices = vertices;
		this->indices = indices;
		this->textures = textures;

		/* set vertex buffer and its attribute pointers */
		setupMesh();
	}

	~Mesh() {
		VBO->destroy();
		EBO->destroy();
	}

	// render the mesh
	void Draw(QOpenGLShaderProgram *shader) {

		// draw mesh
		shader->bind();
			VAO->bind();
				// bind appropriate textures
				unsigned int diffuseNr = 1;
				unsigned int specularNr = 1;
				unsigned int normalNr = 1;
				unsigned int heightNr = 1;
				for (unsigned int i = 0; i < textures.size(); i++) {
					glActiveTexture(GL_TEXTURE0 + i); // active proper texture unit before binding
													    // retrieve texture number (the N in diffuse_textureN)
					string number;
					string name = textures[i].type;
					if (name == "texture_diffuse")
						number = std::to_string(diffuseNr++);
					else if (name == "texture_specular")
						number = std::to_string(specularNr++); // transfer unsigned int to stream
					else if (name == "texture_normal")
						number = std::to_string(normalNr++); // transfer unsigned int to stream
					else if (name == "texture_height")
						number = std::to_string(heightNr++); // transfer unsigned int to stream

					// set the sampler to the correct texture unit					 
					shader->setUniformValue(shader->uniformLocation((name + number).c_str()), i);
					// bind the texture
					textures[i].texture->bind();
				}
				glDrawElements(GL_TRIANGLES, indices.size(), GL_UNSIGNED_INT, 0);
			VAO->release();
		shader->release();
		glActiveTexture(GL_TEXTURE0);
	};

private:
	/* Render data */
	/* Buffer Objects */
	QOpenGLBuffer *VBO;
	QOpenGLBuffer *EBO;

	/* Functions */
	//initializes all the buffer objects/arrays
	void setupMesh()
	{
		initializeOpenGLFunctions();
		//Vertex Array Object
		VAO = new QOpenGLVertexArrayObject();
		VAO->create();
		VAO->bind();

		//Create Buffer
		//Vertex Buffer Object
		VBO = new QOpenGLBuffer(QOpenGLBuffer::VertexBuffer);
		VBO->create();
		VBO->setUsagePattern(QOpenGLBuffer::StaticDraw);
		VBO->bind();
		VBO->allocate(&vertices[0], vertices.size() * sizeof(Vertex));

		// set the vertex attribute pointers
		glEnableVertexAttribArray(0);
		glEnableVertexAttribArray(1);
		glEnableVertexAttribArray(2);
		glEnableVertexAttribArray(3);
		glEnableVertexAttribArray(4);

		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), reinterpret_cast<void*>(offsetof(Vertex, Position)));
		glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), reinterpret_cast<void*>(offsetof(Vertex, Normal)));
		glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), reinterpret_cast<void*>(offsetof(Vertex, TexCoords)));
		glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), reinterpret_cast<void*>(offsetof(Vertex, Tangent)));
		glVertexAttribPointer(4, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), reinterpret_cast<void*>(offsetof(Vertex, Bitangent)));

		//Element Buffer Object
		EBO = new QOpenGLBuffer(QOpenGLBuffer::IndexBuffer);
		EBO->create();
		EBO->setUsagePattern(QOpenGLBuffer::StaticDraw);
		EBO->bind();
		EBO->allocate(&indices[0], indices.size() * sizeof(unsigned int));

		VAO->release();
	}
};
#endif // MESH_H
