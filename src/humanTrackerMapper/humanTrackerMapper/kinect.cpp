#include "kinect.h"
#include <stdexcept>

/* Constructor */
Kinect::Kinect() :
	depthListener(),
	colorListener(),
	irListener() {

	/* Init OpenNI */
	try {
		Kinect::initOpenNI();
	}
	catch (OpenNIException &e) {
		throw e;
	}

	/* Update Devices */
	try {
		Kinect::updateDevices();
	}
	catch (const DeviceException& e) {
		throw e;
	}

	/* Initialize Kinect with ID 0 */
	try {
		Kinect::initKinect(0);
	}
	catch (const DeviceException& e) {
		throw e;
	}

	/* set Intrinsics */
	// focal length
	intrinsics.fx = 365.892f;
	intrinsics.fy = 365.892f; // 390
	// center of intrest
	intrinsics.cx = 251.152f;
	intrinsics.cy = 207.796f;

	// distortion parameters
	intrinsics.k2 = 0.0909336f;
	intrinsics.k4 = -0.268083f;
	intrinsics.k6 = 0.0893225f;
}

/* Destructor */
Kinect::~Kinect() {
	Kinect::deleteCurrentVideoStreams();
	OpenNI::shutdown();
}

/*
 *
 * Interface Implementation
 *
 */

cv::Mat Kinect::getColorMat() {
	/** Camera-Image (color). */
	
	if (device.isValid()) {
		if (!colorListener.receivedFrame()) {
			return cv::Mat();
		}
		cv::Mat colorMat;
		VideoFrameRef frame = colorListener.getFrame();

		if (!colorListener.isFrameCurrent()) {
			throw DeviceException(DeviceExceptionTypes::DE_COLOR_FRAME_NOT_VALID, "No new incoming color Frames!");
		}
		//color.readFrame(&frame);

		if (frame.isValid())
		{
			const openni::RGB888Pixel* imageBuffer = (const openni::RGB888Pixel*)frame.getData();
			colorMat.create(frame.getHeight(), frame.getWidth(), CV_8UC3);
			memcpy(colorMat.data, imageBuffer, 3 * frame.getHeight()*frame.getWidth() * sizeof(uint8_t));
			cv::cvtColor(colorMat, colorMat, CV_BGR2RGB); //this will put colors right
			return colorMat;
		}
		else {
			throw DeviceException(DeviceExceptionTypes::DE_COLOR_FRAME_NOT_VALID, "No valid color Frame");
		}
	}
	else {
		throw DeviceException(DeviceExceptionTypes::DE_DEVICE_NOT_VALID, "The Device is not Valid!");
	}
	return cv::Mat();
}

cv::Mat Kinect::getDepthMat()
{
	if (device.isValid()) {
		if (!depthListener.receivedFrame()) {
			return cv::Mat();
		}
		VideoFrameRef frame = depthListener.getFrame();

		if (!depthListener.isFrameCurrent()) {
			throw DeviceException(DeviceExceptionTypes::DE_DEPTH_FRAME_NOT_VALID, "No new incoming depth Frames!");
		}
		//depth.readFrame(&frame);

		cv::Mat depthMat;
 		depthMat.create(frame.getHeight(), frame.getWidth(), CV_16U);
		
		if (frame.isValid())
		{
			depthMat.data = (uchar*)frame.getData();
			return depthMat;
		}
		else {
			throw DeviceException(DeviceExceptionTypes::DE_DEPTH_FRAME_NOT_VALID, "No valid depth Frame");
		}
	}
	else {
		throw DeviceException(DeviceExceptionTypes::DE_DEVICE_NOT_VALID, "The Device is not Valid!");
	}
	return cv::Mat();
}

cv::Mat Kinect::getIRMat()
{
	if (device.isValid()) {
		if (!irListener.receivedFrame()) {
			return cv::Mat();
		}
		VideoFrameRef frame = irListener.getFrame();

		if (!irListener.isFrameCurrent()) {
			throw DeviceException(DeviceExceptionTypes::DE_DEPTH_FRAME_NOT_VALID, "No new incoming depth Frames!");
		}
		//depth.readFrame(&frame);

		cv::Mat irMat;
		irMat.create(frame.getHeight(), frame.getWidth(), CV_16U);
		//printf("%i, %i", frame.getHeight(), frame.getWidth());
		if (frame.isValid())
		{
			irMat.data = (uchar*)frame.getData();
			return irMat;
		}
		else {
			throw DeviceException(DeviceExceptionTypes::DE_DEPTH_FRAME_NOT_VALID, "No valid depth Frame");
		}
	}
	else {
		throw DeviceException(DeviceExceptionTypes::DE_DEVICE_NOT_VALID, "The Device is not Valid!");
	}
	return cv::Mat();
}

cv::Point3f Kinect::depthToWorld(cv::Point2f depthCoord, float depth) {

	//%projection correction(-1 is for Matlab indexes, for c++ == 0)
	float x = (depthCoord.x - intrinsics.cx - 1.0f) / intrinsics.fx;
	float y = (depthCoord.y - intrinsics.cy - 1.0f) / intrinsics.fy;

	//%distorsion correction
	float x0 = x;
	float y0 = y;

	for (int j = 0; j < 4; j++) {
		float r2 = pow(x, 2) + pow(y, 2);
		float p = 1 / (1 + intrinsics.k2 * r2 + intrinsics.k4 * pow(r2, 2) + intrinsics.k6 * pow(r2, 3));
		x = x0 * p;
		y = y0 * p;
	}

	// convert to meters
	return cv::Point3f(
		x * (float)depth * 0.001f,
		-y * (float)depth * 0.001f,
		(float)depth * 0.001f
	);
};

/*
 * 
 * Public 
 *
 */

/* Init OpenNI Device */
void Kinect::initOpenNI() {
	if (OpenNI::initialize() != STATUS_OK)
	{
		throw OpenNIException(OpenNI::getExtendedError());
	}
}

/* Update Devices */
void Kinect::updateDevices() {
	/* Clear cameras container */
	cameras.clear();

	/* Device enumerator */
	openni::Array<openni::DeviceInfo> deviceInfoList;

	OpenNI::enumerateDevices(&deviceInfoList);

	/* Get amount of Devices listed */
	int amountDevices = deviceInfoList.getSize();

	/* check if at least one device was found */
	if (amountDevices > 0) {
		for (int i = 0; i < amountDevices; ++i)
		{
			DeviceInfo currentDevice = deviceInfoList[i];
			cameras.insert(std::pair<int, openni::DeviceInfo>(i, currentDevice));
		}
	}
	else {
		throw DeviceException(DeviceExceptionTypes::DE_NO_DEVICE, "No connected Kinect camera found!");
	}
}

/* Init Kinect */
void Kinect::initKinect(int cameraIDX) {
	// delete old video streams
	Kinect::deleteCurrentVideoStreams();
	// check if camera ID is valid
	assert(cameraIDX < this->cameras.size());
	// open device with id
	Status rc = device.open(cameras[cameraIDX].getUri());

	// check if opening was  successfull
	if (rc != STATUS_OK)
	{
		throw DeviceException(DeviceExceptionTypes::DE_COULDNT_OPEN_DEVICE, "Device couldn't be opened!");
	}

	// ----- Depth Sensor -----
	if (device.getSensorInfo(SENSOR_DEPTH) != NULL)
	{
		// create depth video stream
		rc = depth.create(device, SENSOR_DEPTH);
		// check if creation was successfull
		if (rc == STATUS_OK)
		{
			// start depth video stream
			rc = depth.start();
			// check if starting was successfull
			if (rc != STATUS_OK)
			{
				throw DeviceException(DE_COULDNT_START_DEPTH_STREAM, "Depth Stream couldn't be started!");
			}
		}
		else
		{
			throw DeviceException(DE_COULDNT_CREATE_DEPTH_STREAM, "Couldn't create Depth Stream!");
		}

		// check if device is a Kinect 2
		if (strcmp(device.getDeviceInfo().getName(), "Kinect 2") == 0) {
			// set video mode with Kinect 2 video parameters
			VideoMode paramvideo;
			paramvideo.setResolution(512, 424);
			paramvideo.setFps(30);
			paramvideo.setPixelFormat(PIXEL_FORMAT_DEPTH_100_UM);
			depth.setVideoMode(paramvideo);
		}
		else
		{
			// set video mode with Kinect video parameters
			VideoMode paramvideo;
			paramvideo.setResolution(640, 480);
			paramvideo.setFps(30);
			paramvideo.setPixelFormat(PIXEL_FORMAT_DEPTH_1_MM);
			depth.setVideoMode(paramvideo);
		}

		// connect framelistener to depth video stream
		depth.addNewFrameListener(&depthListener);
	}
	else {
		throw DeviceException(DE_NO_DEPTH_SENSOR, "No Depth sensor found!");
	}

	// ----- Color Sensor -----
	if (device.getSensorInfo(SENSOR_COLOR) != NULL)
	{
		// create color video stream
		rc = color.create(device, SENSOR_COLOR);
		// check if creation was successfull
		if (rc == STATUS_OK)
		{
			// start color video stream
			rc = color.start();
			// check if starting was successfull
			if (rc != STATUS_OK)
			{
				throw DeviceException(DE_COULDNT_START_COLOR_STREAM, "Color Stream couldn't be started!");
			}
		}
		else
		{
			throw DeviceException(DE_COULDNT_CREATE_COLOR_STREAM, "Couldn't create Color Stream!");
		}

		// check if device is a Kinect 2
		if (strcmp(device.getDeviceInfo().getName(), "Kinect 2") == 0) {
			// set video mode with Kinect 2 video parameters
			VideoMode paramvideo;
			paramvideo.setResolution(1920, 1080);
			paramvideo.setFps(30);
			paramvideo.setPixelFormat(PIXEL_FORMAT_RGB888);
			color.setVideoMode(paramvideo);
		}
		else {
			// set video mode with Kinect video parameters
			VideoMode paramvideo;
			paramvideo.setResolution(640, 480);
			paramvideo.setFps(30);
			paramvideo.setPixelFormat(PIXEL_FORMAT_RGB888);
			color.setVideoMode(paramvideo);
		}

		// connect framelistener to color video stream
		color.addNewFrameListener(&colorListener);
	}
	else {
		throw DeviceException(DE_NO_COLOR_SENSOR, "No Color sensor found!");
	}

	// ----- Infrared Sensor -----
	if (device.getSensorInfo(SENSOR_IR) != NULL)
	{
		// create ir video stream
		rc = ir.create(device, SENSOR_IR);
		// check if creation was successfull
		if (rc == STATUS_OK)
		{
			// start ir video stream
			rc = ir.start();
			// check if starting was successfull
			if (rc != STATUS_OK)
			{
				throw DeviceException(DE_COULDNT_START_IR_STREAM, "IR Stream couldn't be started!");
			}
		}
		else
		{
			throw DeviceException(DE_COULDNT_CREATE_IR_STREAM, "Couldn't create IR Stream!");
		}

		// check if device is a Kinect 2
		if (strcmp(device.getDeviceInfo().getName(), "Kinect 2") == 0) {
			// set video mode with Kinect 2 video parameters
			VideoMode paramvideo;
			paramvideo.setResolution(512, 424);
			paramvideo.setFps(30);
			paramvideo.setPixelFormat(PIXEL_FORMAT_GRAY16);
			ir.setVideoMode(paramvideo);
		}
		else
		{
			// set video mode with Kinect video parameters
			VideoMode paramvideo;
			paramvideo.setResolution(640, 480);
			paramvideo.setFps(30);
			paramvideo.setPixelFormat(PIXEL_FORMAT_GRAY16);
			depth.setVideoMode(paramvideo);
		}

		// connect framelistener to ir video stream
		ir.addNewFrameListener(&irListener);
	}
	else {
		throw DeviceException(DE_NO_IR_SENSOR, "No IR sensor found!");
	}

	//device.setDepthColorSyncEnabled(true);
	//device.setImageRegistrationMode(openni::IMAGE_REGISTRATION_DEPTH_TO_COLOR);
}

std::vector<std::string> Kinect::getCameras() {
	std::vector<std::string> res;
	/* TODO WARUM ist die kinect 2 3x drinne */

	for (auto cam : this->cameras) {
		res.push_back(cam.second.getName());
	}

	return res;
}

/*
 *
 * Private
 *
 */
void Kinect::deleteCurrentVideoStreams() {
	depth.stop();
	depth.destroy();
	color.stop();
	color.destroy();
	ir.stop();
	ir.destroy();
	device.close();
}

