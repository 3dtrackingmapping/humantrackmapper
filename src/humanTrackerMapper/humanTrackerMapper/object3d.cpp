#include "object3d.h"

/* Constructor */
Object3D::Object3D(QOpenGLShaderProgram *shader)
{
	initializeOpenGLFunctions();
	vertices = {
		-0.5f, -0.5f, -0.5f,  1.0f, 0.0f, 0.0f,
		0.5f,  0.5f, -0.5f,  1.0f, 0.0f, 0.0f,
		0.5f, -0.5f, -0.5f,  1.0f, 0.0f, 0.0f,
		0.5f,  0.5f, -0.5f,  1.0f, 0.0f, 0.0f,
		-0.5f, -0.5f, -0.5f,  1.0f, 0.0f, 0.0f,
		-0.5f,  0.5f, -0.5f,  1.0f, 0.0f, 0.0f,

		-0.5f, -0.5f,  0.5f,  0.0f, 1.0f, 0.0f,
		 0.5f, -0.5f,  0.5f,  0.0f, 1.0f, 0.0f,
		 0.5f,  0.5f,  0.5f,  0.0f, 1.0f, 0.0f,
		 0.5f,  0.5f,  0.5f,  0.0f, 1.0f, 0.0f,
		-0.5f,  0.5f,  0.5f,  0.0f, 1.0f, 0.0f,
		-0.5f, -0.5f,  0.5f,  0.0f, 1.0f, 0.0f,

		-0.5f,  0.5f,  0.5f,  0.0f, 0.0f, 1.0f,
		-0.5f,  0.5f, -0.5f,  0.0f, 0.0f, 1.0f,
		-0.5f, -0.5f, -0.5f,  0.0f, 0.0f, 1.0f,
		-0.5f, -0.5f, -0.5f,  0.0f, 0.0f, 1.0f,
		-0.5f, -0.5f,  0.5f,  0.0f, 0.0f, 1.0f,
		-0.5f,  0.5f,  0.5f,  0.0f, 0.0f, 1.0f,

		0.5f,  0.5f,  0.5f,  1.0f, 1.0f, 0.0f,
		0.5f, -0.5f, -0.5f,  1.0f, 1.0f, 0.0f,
		0.5f,  0.5f, -0.5f,  1.0f, 1.0f, 0.0f,
		0.5f, -0.5f, -0.5f,  1.0f, 1.0f, 0.0f,
		0.5f,  0.5f,  0.5f,  1.0f, 1.0f, 0.0f,
		0.5f, -0.5f,  0.5f,  1.0f, 1.0f, 0.0f,

		-0.5f, -0.5f, -0.5f,  1.0f, 0.0f, 1.0f,
		 0.5f, -0.5f, -0.5f,  1.0f, 0.0f, 1.0f,
		 0.5f, -0.5f,  0.5f,  1.0f, 0.0f, 1.0f,
		 0.5f, -0.5f,  0.5f,  1.0f, 0.0f, 1.0f,
		-0.5f, -0.5f,  0.5f,  1.0f, 0.0f, 1.0f,
		-0.5f, -0.5f, -0.5f,  1.0f, 0.0f, 1.0f,

		-0.5f,  0.5f, -0.5f,  1.0f, 1.0f, 1.0f,
		 0.5f,  0.5f,  0.5f,  1.0f, 1.0f, 1.0f,
		 0.5f,  0.5f, -0.5f,  1.0f, 1.0f, 1.0f,
		 0.5f,  0.5f,  0.5f,  1.0f, 1.0f, 1.0f,
		-0.5f,  0.5f, -0.5f,  1.0f, 1.0f, 1.0f,
		-0.5f,  0.5f,  0.5f,  1.0f, 1.0f, 1.0f
	};

	shader->bind();
	// get Location
	mModel = shader->uniformLocation("model");
	
	//Create Buffer 
	vbo.create();
	vbo.bind();
	vbo.setUsagePattern(QOpenGLBuffer::StaticDraw);
	vbo.allocate(vertices.data(), vertices.size()*sizeof(float));

	//VertexArrayObject
	vao.create();
	vao.bind();
	shader->enableAttributeArray(0);
	shader->enableAttributeArray(5);

	//set Bufferattributes
	shader->setAttributeBuffer(0, GL_FLOAT,					0,	3,	6 * sizeof(float));
	shader->setAttributeBuffer(5, GL_FLOAT, 3 * sizeof(float),	3,	6 * sizeof(float));
	
	vao.release();
	vbo.release();
	shader->release();
}

void Object3D::setPosition(QVector3D pos)
{
	position = pos;
}

void Object3D::setRotation(QVector3D rot)
{
	rotation = rot;
}

void Object3D::setScale(QVector3D sca)
{
	scale = sca;
}

QVector3D Object3D::getPosition()
{
	return position;
}

QVector3D Object3D::getRotation()
{
	return rotation;
}

QVector3D Object3D::getScale()
{
	return scale;
}

/* Draw 3D Object */
void Object3D::draw(QOpenGLShaderProgram *shader)
{
	shader->bind();
		vao.bind();
		QMatrix4x4 matrix;
		matrix.setToIdentity();
		matrix.translate(position.x(), position.y(), position.z());
		matrix.rotate(rotation.z(), 0.0, 0.0, 1.0);
		matrix.rotate(rotation.y(), 0.0, 1.0, 0.0);
		matrix.rotate(rotation.x(), 1.0, 0.0, 0.0);
		matrix.scale(scale.x(), scale.y(), scale.z());

		shader->setUniformValue(mModel, matrix);
		glDrawArrays(GL_TRIANGLES, 0, vertices.size()/6);
		vao.release();
	shader->release();
}

/* Destroy Object & free memory */
void Object3D::destroy()
{
	vao.destroy();
	vbo.destroy();
}
