#include "captureFrameListener.h"
#include <iostream>

void CaptureFrameListener::onNewFrame(openni::VideoStream & stream)
{
	frameReceived = true;

	stream.readFrame(&frame);

	timeLastFrame = std::chrono::system_clock::now();
}

VideoFrameRef CaptureFrameListener::getFrame() {
	return frame;
}

bool CaptureFrameListener::isFrameCurrent()
{
	auto end = std::chrono::system_clock::now();
	std::chrono::duration<double> elapsed_seconds = std::chrono::system_clock::now() - timeLastFrame;

	return elapsed_seconds.count() < MAX_WAIT_TIME || !frameReceived;
}

bool CaptureFrameListener::receivedFrame() {
	return frameReceived;
}

