#ifndef FACELANDMARKS_H
#define FACELANDMARKS_H

#include <opencv2/opencv.hpp>

class FaceLandmarks {
public:
	/**/
	enum FLM {
		FLM_01 = 0,
		FLM_02,
		FLM_03,
		FLM_04,
		FLM_05,
		FLM_06,
		FLM_07,
		FLM_08,
		FLM_CHIN_POINT,				// 09
		FLM_10,
		FLM_11,
		FLM_12,
		FLM_13,
		FLM_14,
		FLM_15,
		FLM_16,
		FLM_17,
		FLM_EYE_BROWN_RIGHT_RIGHT,	// 18
		FLM_19,
		FLM_EYE_BROWN_RIGHT_CENTER,	// 20
		FLM_21,
		FLM_EYE_BROWN_RIGHT_LEFT,	// 22
		FLM_EYE_BROWN_LEFT_RIGHT,	// 23
		FLM_24,
		FLM_EYE_BROWN_LEFT_CENTER,	// 25
		FLM_26,
		FLM_EYE_BROWN_LEFT_LEFT,	// 27
		FLM_NOSE_DORSUM_TOP,		// 28
		FLM_NOSE_DORSUM_CENTER,		// 29
		FLM_NOSE_DORSUM_BOTTOM,		// 30
		FLM_NOSE_TIP,				// 31
		FLM_NOSE_BOTTOM_RIGHT,		// 32
		FLM_33,
		FLM_PHILITRUM_CENTER,		// 34
		FLM_35,
		FLM_NOSE_BOTTOM_LEFT,		// 36
		FLM_EYE_RIGHT_CORNER_RIGHT,	// 37
		FLM_38,
		FLM_39,
		FLM_EYE_RIGHT_CORNER_LEFT,	// 40
		FLM_41,
		FLM_42,
		FLM_EYE_LEFT_CORNER_RIGHT,	// 43
		FLM_44,
		FLM_45,
		FLM_EYE_LEFT_CORNER_LEFT,	// 46
		FLM_47,
		FLM_48,
		FLM_MOUTH_CORNER_RIGHT,		// 49
		FLM_50,
		FLM_51,
		FLM_MOUTH_TOP,				// 52
		FLM_53,
		FLM_54,
		FLM_MOUTH_CORNER_LEFT,		// 55
		FLM_56,
		FLM_57,
		FLM_MOUTH_BOTTOM,			// 58
		FLM_59,
		FLM_60,
		FLM_LIPS_CORNER_RIGHT,		// 61
		FLM_62,
		FLM_LIPS_TOP,				// 63
		FLM_64,
		FLM_LIPS_CORNER_LEFT,		// 64
		FLM_66,
		FLM_LIPS_BOTTOM,			// 67
		FLM_68,
		FLM_COUNT_POINTS			// 68 Points in total
	};

	FaceLandmarks() {
	};

	void setFaceLandmarks(std::vector<cv::Point2f> landmarks) {
		faceLandmarks = landmarks;
	};

	cv::Point2f getLandmark(int idx) {
		if (faceLandmarksFound && idx < faceLandmarks.size()) {
			return faceLandmarks[idx];
		}
		else {

			return cv::Point2f();
		}

	}

	std::vector<cv::Point2f> getFaceLandmarks() {
		if (faceLandmarksFound) {
			return faceLandmarks;
		}
		else {

			return std::vector<cv::Point2f>();
		}
	};
	
	void setFound(bool found) {
		faceLandmarksFound = found;
	}

	bool foundLandmarks() {
		return faceLandmarksFound;
	}

private:
	/* found in current loop */
	bool faceLandmarksFound = false;
	
	std::vector<cv::Point2f> faceLandmarks;
	std::vector<cv::Point3f> faceLandmarkWorld;


};

#endif // !FACELANDMARKS_H
