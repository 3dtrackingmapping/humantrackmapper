#ifndef EXCEPTION_H
#define EXCEPTION_H

#include <exception>
#include <iostream>

/** Types for DeviceException */
typedef enum {
	DE_NO_DEVICE,
	DE_DEVICE_NOT_VALID,
	DE_COULDNT_OPEN_DEVICE,
	DE_NO_DEPTH_SENSOR,
	DE_NO_COLOR_SENSOR,
	DE_NO_IR_SENSOR,
	DE_COULDNT_START_DEPTH_STREAM,
	DE_COULDNT_START_COLOR_STREAM,
	DE_COULDNT_START_IR_STREAM,
	DE_COULDNT_CREATE_DEPTH_STREAM,
	DE_COULDNT_CREATE_COLOR_STREAM,
	DE_COULDNT_CREATE_IR_STREAM,
	DE_COLOR_FRAME_NOT_VALID,
	DE_DEPTH_FRAME_NOT_VALID,
	DE_UNKNOWN_ERROR,
	DE_NUM_TYPES
} DeviceExceptionTypes;

/**
 * Class contains definition of new exception for an OpenNi Error.
 */
class OpenNIException : public std::exception {
private:
	/** Message of Exception. */
	std::string message;
public:
	/** 
	 * Constructor of exception.
	 * @param message Exception message
	 */
	explicit OpenNIException(const std::string& message) :message(message) {};
	/**
	 * Returns the exception message.
	 * @return message
	 */
	virtual const char* what() const throw() {
		return message.c_str();
	}
};

/**
 * Class contains definition of new exception for an Device Error.
 */
class DeviceException : public std::exception {
private:
	/** Message of Exception. */
	std::string message;
	/** Type of Exception. */
	DeviceExceptionTypes type = DE_UNKNOWN_ERROR;

public:
	/**
	 * Constructor of exception.
	 * @param type Type of exception
	 * @param message Exception message
	 */
	explicit DeviceException(DeviceExceptionTypes type, const std::string& message) : type(type), message(message) {};
	/**
	 * Returns the exception message.
	 * @return message
	 */
	virtual const char* what() const throw() {
		return message.c_str();
	}
	/**
	 * Returns the type of the exception.
	 * @return exception type
	 */
	DeviceExceptionTypes getType() { return type; };
};


#endif // EXCEPTION_H