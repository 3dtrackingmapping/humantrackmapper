#ifndef CAPTURE_INTERFACE
#define CAPTURE_INTERFACE

// OpenCV
#include <opencv\highgui.h>
#include <opencv\cv.h>
#include <opencv2\opencv.hpp>

/**
 * General Interface for a Camera Source. 
 */
class CaptureInterface
{
public:
	typedef struct {
		float fx;
		float fy;
		float cx;
		float cy;

		float k2;
		float k4;
		float k6;
	} CameraIntrinsics;

	/**
	 * Delivers the color image of the Camera.
	 * @return color image
	 */
	virtual cv::Mat getColorMat() = 0;

	/**
	 * Delivers the color image of the Camera.
	 * @return color image
	 */
	virtual cv::Mat getDepthMat() = 0; 

	/**
	 * Delivers the IR-image of the Camera.
	 * @return IR-image
	 */
	virtual cv::Mat getIRMat() = 0;

	/**
	 * Converts the Depth Value to Wolrd Data 
	 * @return world Coordinate xyz
	 */
	virtual cv::Point3f depthToWorld(cv::Point2f depthCoord, float depth) = 0;
};

#endif // !CAPTURE_INTERFACE