#ifndef IMAGE_PROCESSING_H
#define IMAGE_PROCESSING_H

#define HUMAN_HEAD_WIDTH 130
#define HUMAN_HEAD_HEIGHT 150

// Percentage samples tp throw away
// half on each side of the samples
#define CUTOFF_PERCENTAGE 40
#define DISTANCE_THRESHOLD 100
#define DISTANCE_NEAR_BOUND 200
#define DISTANCE_FAR_BOUND 160

#define focalLength 365

// OpenCV
#include <opencv\highgui.h>
#include <opencv\cv.h>
#include <opencv2\opencv.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include "faceLandmarks.h"

//System Includes 
#include <string>
#include <iostream>
#include <stdio.h>
#include <algorithm>

static int lastAverageFaceDepth = 0;

/**
 * Creates a binary mask of the detected face region.
 *
 * @param inputImage Depth 16 Bit
 * @param facemarks Landmarks
 * @param faceRegion detected face region
 * @param croppedDepthImageLarge detected scaled face region image
 * @param calcAverageFaceDepth defines if facedepth should be recalculated
 */
static cv::Mat createBinaryMask(cv::Mat inputImage, FaceLandmarks * facemarks, cv::Rect faceRegion, cv::Mat croppedDepthImageLarge, bool calcAverageFaceDepth) {
	int averageFaceDepth = 0;

	if (calcAverageFaceDepth) { // if aver Depth shal be calced by marks
		int idx = 0;
		std::multimap<int, int> depthValues; //<depth, idx of Landmark>

		std::vector<cv::Point2f> lM = facemarks->getFaceLandmarks();

		for (cv::Point2f point : lM) {
			if (point.x >= 0 && point.y >= 0 && point.x < inputImage.cols && point.y < inputImage.rows)
			{
				int value = inputImage.at<unsigned short>(point.y, point.x);

				if (value > 0) {
					depthValues.insert(std::make_pair(value, idx));
					// insert in sorted array
				}
				idx++;
			}
		}

		int numDepthValues = depthValues.size();
		int numCutOffValuesEachSide = (numDepthValues * (CUTOFF_PERCENTAGE/100)) / 2;
		idx = 0;

		for (auto const& depthValue : depthValues) {
			if (idx >= numCutOffValuesEachSide && idx < numDepthValues - numCutOffValuesEachSide) {
				averageFaceDepth += depthValue.first;
			}
			idx++;
		}

		if (numDepthValues - (numCutOffValuesEachSide * 2) > 0) {
			averageFaceDepth /= numDepthValues - (numCutOffValuesEachSide * 2);
		}
		lastAverageFaceDepth = averageFaceDepth;
	}
	else {
		averageFaceDepth = lastAverageFaceDepth;
	}

	int centerAverage = 0;
	int area = 9; //Area < image
	int colStart = croppedDepthImageLarge.cols / 2 - area / 2;
	int rowStart = croppedDepthImageLarge.rows / 2 - area / 2;
	for (int row = rowStart; row < rowStart + area; row++) {
		for (int col = colStart; col < colStart + area; col++) {
			centerAverage += croppedDepthImageLarge.at<unsigned short>(row, col);
		}
	}
	centerAverage /= pow(area, 2);

	if (abs(averageFaceDepth - centerAverage) > DISTANCE_THRESHOLD) {
		averageFaceDepth = centerAverage;
	}

	int NEAR_CLIPPING = std::max(averageFaceDepth - DISTANCE_NEAR_BOUND, 1);
	int FAR_CLIPPING = averageFaceDepth + DISTANCE_FAR_BOUND;

	//Binary Mask
	cv::Mat binaryMask = cv::Mat::zeros(croppedDepthImageLarge.size(), CV_8U);
	if (lastAverageFaceDepth > 0) {
		cv::inRange(croppedDepthImageLarge, NEAR_CLIPPING, FAR_CLIPPING, binaryMask);
	}
	
	std::vector<std::vector<cv::Point> > contours;
	std::vector<cv::Vec4i> hierarchy;

	//Cut off body with ellipse shape
	// Find contours
	cv::findContours(binaryMask, contours, hierarchy, CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE, cv::Point(0, 0));
	
	cv::Mat binaryMask2 = cv::Mat::zeros(croppedDepthImageLarge.size(), CV_8U);

	if (averageFaceDepth > 0) {

		cv::Size size(365* HUMAN_HEAD_WIDTH / averageFaceDepth, 365 * HUMAN_HEAD_HEIGHT / averageFaceDepth);


		cv::ellipse(binaryMask2, 
			cv::Point2f(croppedDepthImageLarge.cols/2, croppedDepthImageLarge.rows/2),
			size,
			0,0,360, 255, -1, 8);

	}
	//Combine binary mats
	cv::Mat binaryMaskCombined = cv::Mat::zeros(croppedDepthImageLarge.size(), CV_8U);

	for (int row = 0; row < binaryMaskCombined.rows; row++) {
		for (int col = 0; col < binaryMaskCombined.cols; col++) {
			if (binaryMask.at<unsigned char>(row, col) == 255 && binaryMask2.at<unsigned char>(row, col) == 255) {
				binaryMaskCombined.at<unsigned char>(row, col) = binaryMask.at<unsigned char>(row, col);
			}
		}
	}

	return binaryMaskCombined;
}


#endif //!IMAGE_PROCESSING_H