#include "humanTrackerMapper.h"
#include <QtWidgets/QApplication>
#include "kinect.h"

/**
 * Main Method opens MainWindow.
 */
int main(int argc, char *argv[])
{
	
	QApplication application(argc, argv);

	//Hauptfenster
	HumanTrackerMapper mainWindow;
	mainWindow.showMaximized();
	mainWindow.show();
	
	return application.exec();
}