#ifndef OBJECT3D_H
#define OBJECT3D_H
#include <QOpenGLFunctions>
#include <qopenglshaderprogram.h>
#include <qvector3d.h>
#include <qopenglvertexarrayobject.h>
#include <qopenglbuffer.h>
#include <vector>

using namespace std;

class Object3D : protected QOpenGLFunctions {
public:
	explicit Object3D(QOpenGLShaderProgram *shader);
	/* Setter */
	void setPosition(QVector3D pos);
	void setRotation(QVector3D rot);
	void setScale(QVector3D sca);
	/* Getter */
	QVector3D getPosition();
	QVector3D getRotation();
	QVector3D getScale();

	void draw(QOpenGLShaderProgram *shader);
	void destroy();

private:
	vector<GLfloat> vertices;
	QVector3D position = { 0.0, 0.0, 0.0 };
	QVector3D scale = { 1.0, 1.0, 1.0 };
	QVector3D rotation = { 0.0, 0.0, 0.0 };

	GLuint mModel;

	/* Buffer Objects */
	QOpenGLBuffer vbo;
	QOpenGLVertexArrayObject vao;
};

#endif // !OBJECT3D_H
