#ifndef QTOPENCVVIEWERGL_H
#define QTOPENCVVIEWERGL_H

// Qt 
#include <QtGui/QGuiApplication>
#include <QOpenGLWidget>
#include <QGLBuffer>
#include <QGLformat>
#include <QOpenGLFunctions>
#include <QtCore/qmath.h>
#include <QtGui/QMatrix4x4>
#include <QtGui/QOpenGLShaderProgram>

// OpenCV
#include <opencv2/opencv.hpp>
#include <opencv2/core/core.hpp>

#include <mutex>

// 3D
#include "pointLight.h"
#include "object3d.h"
#include "camera.h"
#include "model.h"

/**
 * Class implements new QWidget, which shows Camera-Image and 3D Object.
 */
class MappingWidget : public QOpenGLWidget, protected QOpenGLFunctions
{
	Q_OBJECT
public:
	/** Constructor */
	explicit MappingWidget(QWidget *parent = 0);
	/** Change position of 3D Object */
	void setObjectPosition(float x, float y, float z);
	/** Change rotation */
	void setObjectRotation(cv::Point3f dirVec);
	/** Set image to show as Background */
	bool showImage(const cv::Mat& image);

	/* Projection matrix FOV values */
	float fovX = 96;
	float fovY = 54;

	Camera *cam;									///Camera
public slots:
	/* Sets Intensity of 3D Object. */
	void setIntensity(int intensity);

signals:
	/** Change image size, when widget size is changed */
	void imageSizeChanged(int outW, int outH);

protected:
	/** initialize OpenGL */
	void initializeGL();							/// OpenGL initialization
	void paintGL();									/// OpenGL Rendering
	void resizeGL(int width, int height);			/// Widget Resize Event

	void        updateScene();
	void        renderImage();
	void		draw3DScene();

private:

	QImage      mRenderQtImg;						/// Qt image to be rendered
	QImage      mResizedImg;
	cv::Mat     mOrigImage;							/// original OpenCV image to be shown

	QColor      mBgColor;							/// Background color

	float       mImgRatio;							/// height/width ratio

	/* Shader */
	QOpenGLShaderProgram *mShader;					/// Shader Programm

	/* Shader Locations */
	GLuint mProjection;										
	GLuint mView;
	GLuint mIntensity;
	GLuint mModel;
	GLuint mViewPos;
	GLuint mObjectColor;

	/* 3D Object */

	// Only DEBUG
	Object3D *object;								///3D Object
	// ! 

	Model *model;									///3D Model


	PointLight *light;								///Light(s)

	int mWidth;
	int mHeight;

	float intensity3D = 1.0;						///Intensity

	int mRenderWidth;
	int mRenderHeight;
	int mRenderPosX;
	int mRenderPosY;

	void recalculatePosition();

	std::mutex drawMutex;

};

#endif // QTOPENCVVIEWERGL_H
