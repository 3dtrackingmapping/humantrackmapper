#ifndef humanTrack3DWindow
#define humanTrack3DWindow

#include "OpenGLWindow.h"
#include <QtGui/QGuiApplication>
#include <QtGui/QMatrix4x4>
#include <QtGui/QOpenGLShaderProgram>
#include <QtGui/QScreen>
#include <QDesktopWidget>

#include <QtCore/qmath.h>

#include <QtWidgets/QApplication>

class Window3D : public OpenGLWindow
{
public:
	Window3D();

	void initialize() override;
	void render() override;

private:
	GLuint m_posAttr;
	GLuint m_colAttr;
	GLuint m_matrixUniform;

	QOpenGLShaderProgram *m_shaderProgram;
	QOpenGLShaderProgram *m_program;
	int m_frame;
};

#endif //humanTack3DWindow