#include "faceRegionDetection.h"

FaceRegionDetection::FaceRegionDetection() :
	faceDetector(), facemark(), averageArea(0), detections(0), detectedFaceRegion(), hasFaceRegion(false),
	currentTracker(TRACKER_MIL), isFaceTrackerInitialized(false), trackedFaceRegion(), showLandmarks(true),
	showDetectedFaceRegion(true), showTrackedFaceRegion(true) {

	initFaceDetector();
}

void FaceRegionDetection::initFaceDetector() {
	facemark = face::FacemarkLBF::create();
	facemark->loadModel("Ressources/ObjectDetection/lbfmodel.yaml");

	if (!faceDetector.load("Ressources/ObjectDetection/haarcascade_frontalface_alt2.xml"))
	{
		cerr << "Face cascade could not be loaded!" << endl;
	};
}

void FaceRegionDetection::initFaceTracker(Rect2d faceRegion, cv::Mat& frame) {
	setTrackerType(currentTracker);

	// initial FaceRegion Bounding Box 
	trackedFaceRegion = faceRegion;
	tracker->init(frame, trackedFaceRegion);
	isFaceTrackerInitialized = true;
}

cv::Rect FaceRegionDetection::detectFaceRegion(cv::Mat& inputFrame, cv::Mat& outputVisualization, bool& faceRegionAvailable, FaceLandmarks * landmarks) {
	cv::Rect region;
	bool isAccurate = false;
	vector<vector<Point2f>> allLandmarks;

	faceRegionAvailable = detectFaceRegion(inputFrame, outputVisualization, region, isAccurate, true, allLandmarks);

	if (allLandmarks.size() > 0) {
		landmarks->setFaceLandmarks(allLandmarks.front());
		landmarks->setFound(true);
	}

	if (faceRegionAvailable && isAccurate && isRectInsideImage(inputFrame, region)) {
		initFaceTracker(region, inputFrame);
	}

	if (trackFaceRegion(inputFrame, outputVisualization)) {
		region = trackedFaceRegion;
	}

	return region;
}

bool FaceRegionDetection::detectFaceRegion(	cv::Mat& inputFrame, 
											cv::Mat& outputVisualization, 
											cv::Rect& faceRegion, 
											bool& isAccurate, 
											bool increaseWhenInaccurate, 
											vector<vector<Point2f>>& landmarks) {
	Mat frameGray;


	if (inputFrame.channels() == 1) {
		frameGray = inputFrame.clone();
	} else if(inputFrame.channels() == 3) {
		/* if image is color image */
		cvtColor(inputFrame, frameGray, COLOR_BGR2GRAY);
	}
	else {
		printf("Detect Face Region - Input not yet Supported!");
	}
	equalizeHist(frameGray, frameGray);

	vector<Rect> faces;
	vector<int> numDetections;

	isAccurate = false;
	faceRegion = detectedFaceRegion;


	faceDetector.detectMultiScale(frameGray, faces, numDetections, 1.1, 10);
	for (int i = 0; i < faces.size(); i++)
	{
		int currentFaceArea = faces[i].area();
		averageArea = (detections * averageArea + currentFaceArea * inputWeight) / (detections + inputWeight + 1);
		detections++;

		float variance = averageArea * acceptableVariance;

		if ((currentFaceArea > averageArea - variance) && (currentFaceArea < averageArea + variance)) {
			faceRegion = faces[i];

			cv::Size scaledSize(faceRegion.width * faceScaleFactor, faceRegion.height * faceScaleFactor);
			cv::Point offset(scaledSize.width / 2, scaledSize.height / 2);
			faceRegion += scaledSize;
			faceRegion -= offset;

			hasFaceRegion = true;
			isAccurate = true;

			detectedFaceRegion = faceRegion;
		}
	}
	

	//find landmarks
	vector<vector<Point2f>> newLandmarks;
	vector<cv::Rect> foundFaceRegion = {faceRegion};

	// Run landmark detector
	if (hasFaceRegion && facemark->fit(inputFrame, foundFaceRegion, newLandmarks))
	{
		if (showLandmarks) {
			// If successful, render the landmarks on the face
			for (int i = 0; i < newLandmarks.size(); i++)
			{
				drawLandmarks(outputVisualization, newLandmarks[i]);
			}
		}
		landmarks = newLandmarks;
	}
	
	if (!isAccurate && increaseWhenInaccurate) {
		cv::Size scaledSize(faceRegion.width * inacurrateIncreaseFactor, faceRegion.height * inacurrateIncreaseFactor);
		cv::Point offset(scaledSize.width / 2, scaledSize.height / 2);
		faceRegion += scaledSize;
		faceRegion -= offset;
	}

	if (hasFaceRegion && showDetectedFaceRegion) {
		if (isAccurate) {
			rectangle(outputVisualization, faceRegion, COLOR_GREEN, 3);
		}
		else {
			rectangle(outputVisualization, faceRegion, COLOR_RED, 1);
		}
	}

	return hasFaceRegion;
}

void FaceRegionDetection::setTrackerType(TrackerType eTrackerType)
{
#if (CV_MINOR_VERSION < 3)
	{
		tracker = Tracker::create(trackerTypeToString(currentTracker));
	}
#else
	{
		//since OpenCV 3.4.1 � BOOSTING, MIL, KCF, TLD, MEDIANFLOW, GOTURN, MOSSE, CSRT
		//OpenCV 3.2 - BOOSTING, MIL, TLD, MEDIANFLOW, MOSSE
		//OpenCV 3.1 - BOOSTING, MIL, TLD, MEDIANFLOW, KCF
		//OpenCV 3.0 - BOOSTING, MIL, TLD, MEDIANFLOW

		switch (eTrackerType) {
		case TRACKER_BOOSTING:
			tracker = TrackerBoosting::create();
			break;
		case TRACKER_CSRT:
			tracker = TrackerCSRT::create();
			break;
		case TRACKER_KCF:
			tracker = TrackerKCF::create();
			break;
		case TRACKER_MEDIANFLOW:
			tracker = TrackerMedianFlow::create();
			break;
		case TRACKER_MIL:
			tracker = TrackerMIL::create();
			break;
		case TRACKER_MOUSSE:
			tracker = TrackerMOSSE::create();
			break;
		case TRACKER_TLD:
			tracker = TrackerTLD::create();
			break;
		case TRAKER_GOTURN:
			tracker = TrackerGOTURN::create();
			break;
		}
	}
#endif
}

void FaceRegionDetection::setShowLandmarks(bool status)
{
	showLandmarks = status;
}

void FaceRegionDetection::setShowDetectedFaceRegion(bool status)
{
	showDetectedFaceRegion = status;
}

void FaceRegionDetection::setShowTrackedFaceRegion(bool status)
{
	showTrackedFaceRegion = status;
}

void FaceRegionDetection::disableVisualization()
{
	setShowDetectedFaceRegion(false);
	setShowLandmarks(false);
	setShowTrackedFaceRegion(false);
}

void FaceRegionDetection::drawLandmarks(Mat &im, vector<Point2f> &landmarks)
{
	if (landmarks.size() == 68)
	{
		drawPolyline(im, landmarks, 0, 16);           // Jaw line
		drawPolyline(im, landmarks, 17, 21);          // Left eyebrow
		drawPolyline(im, landmarks, 22, 26);          // Right eyebrow
		drawPolyline(im, landmarks, 27, 30);          // Nose bridge
		drawPolyline(im, landmarks, 30, 35, true);    // Lower nose
		drawPolyline(im, landmarks, 36, 41, true);    // Left eye
		drawPolyline(im, landmarks, 42, 47, true);    // Right Eye
		drawPolyline(im, landmarks, 48, 59, true);    // Outer lip
		drawPolyline(im, landmarks, 60, 67, true);    // Inner lip
	}
	else
	{
		// unknown which point correspond to which facial feature -> only dots per Landmark
		for (int i = 0; i < landmarks.size(); i++)
		{
			circle(im, landmarks[i], 3, COLOR_LIGHT_BLUE, FILLED);
		}
	}
}

void FaceRegionDetection::drawPolyline(Mat &im, const vector<Point2f> &landmarks, const int start, const int end, bool isClosed)
{
	// Gather all points between the start and end indices
	vector <Point> points;
	for (int i = start; i <= end; i++)
	{
		points.push_back(cv::Point(landmarks[i].x, landmarks[i].y));
	}
	// Draw polylines. 
	polylines(im, points, isClosed, COLOR_LIGHT_BLUE, 2, 16);
}

bool FaceRegionDetection::trackFaceRegion(cv::Mat& inputFrame, cv::Mat& outputVisualization) {
	bool success = false;

	if (isFaceTrackerInitialized) {
		if (tracker->update(inputFrame, trackedFaceRegion))
		{
			if (showTrackedFaceRegion) {
				rectangle(outputVisualization, trackedFaceRegion, COLOR_BLUE, 2, 1);
			}

			success = true;
		}
	}
	return success;
}

string FaceRegionDetection::trackerTypeToString(TrackerType eType) {
	string typeCaption = "Undefined";

	switch (eType) {
	case TRACKER_BOOSTING:
		typeCaption = "BOOSTING";
		break;
	case TRACKER_CSRT:
		typeCaption = "CSRT";
		break;
	case TRACKER_KCF:
		typeCaption = "KCF";
		break;
	case TRACKER_MEDIANFLOW:
		typeCaption = "MEDIANFLOW";
		break;
	case TRACKER_MIL:
		typeCaption = "MIL";
		break;
	case TRACKER_MOUSSE:
		typeCaption = "MOUSSE";
		break;
	case TRACKER_TLD:
		typeCaption = "TLD";
		break;
	case TRAKER_GOTURN:
		typeCaption = "GOTURN";
		break;
	}

	return typeCaption;
}

bool FaceRegionDetection::isRectInsideImage(const Mat & im, const Rect2d & rect)
{
	return rect.x >= 0 && rect.x + rect.width <= im.cols && rect.y >= 0 && rect.y + rect.height <= im.rows;
}