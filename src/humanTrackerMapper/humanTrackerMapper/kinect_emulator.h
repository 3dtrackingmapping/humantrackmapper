#ifndef KINECT_EMU_H
#define KINECT_EMU_H

// Assertions
#include <assert.h>

// Exceptions
#include "exception.h"
#include <stdexcept>

#include <OpenNI.h>
#include <direct.h>
#include "captureInterface.h"
#include "captureFrameListener.h"

/**
 * default path
 */
const std::string path = "Ressources/SavedImages/";

/**
 * default file extension
 */
const std::string fileFormat = ".png";

/**
 * Class emulates a Camera, by using a saved Dataset instead of a camera VideoStream.
 */
class Kinect_EMU : public CaptureInterface {
public:
	/*
	 * constructor
	 * @param file - name to file
	 */
	Kinect_EMU(std::string file) {
		
		// ----- Depth Sensor -----
		// create depth file path
		std::string depthPath = path;
		depthPath.append(file).append("_d").append(fileFormat);
		// read the file to mat depth
		depth = cv::imread(depthPath, CV_LOAD_IMAGE_ANYDEPTH);
		// check if reading was successfull
		if (!depth.data)
		{
			cout << "Could not open or find the image depth" << std::endl;
			exit(1);
		}

		// ----- Color Sensor -----
		// create color file path
		std::string colorPath = path;
		colorPath.append(file).append(fileFormat);
		// read the file to mat color
		color = cv::imread(colorPath, CV_LOAD_IMAGE_ANYCOLOR);
		// check if reading was successfull
		if (!color.data)                              // Check for invalid input
		{
			cout << "Could not open or find the image color" << std::endl;
			exit(1);
		}

		// ----- Infrared Sensor -----
		// create ir file path
		std::string irPath = path;
		irPath.append(file).append("_ir").append(fileFormat);
		// read the file to mat ir
		ir = cv::imread(irPath, CV_LOAD_IMAGE_ANYDEPTH);
		// check if reading was successfull
		if (!ir.data)
		{
			cout << "Could not open or find the image ir" << std::endl;
			exit(1);
		}

		/* set Intrinsics */
		// focal length
		intrinsics.fx = 365.892f;
		intrinsics.fy = 390; // check
		// center of intrest
		intrinsics.cx = 251.152f;
		intrinsics.cy = 207.796f;

		// distortion parameters
		intrinsics.k2 = 0.0909336f;
		intrinsics.k4 = -0.268083f;
		intrinsics.k6 = 0.0893225f;
	};

	/**
	 * destructor
	 */
	~Kinect_EMU() {};

	/**
	 * Delivers the color image of the Camera.
	 * @return color image
	 */
	cv::Mat getColorMat() override {
		return color;
	};
	/**
	 * Delivers the color image of the Camera.
	 * @return color image
	 */
	cv::Mat getDepthMat() override {
		return depth;
	};
	/**
	 * Delivers the IR-image of the Camera.
	 * @return IR-image
	 */
	cv::Mat getIRMat() override {
		return ir;
	};

	cv::Point3f depthToWorld(cv::Point2f depthCoord, float depth) override {

		//%projection correction(-1 is for Matlab indexes, for c++ == 0)
		float x = (depthCoord.x - intrinsics.cx - 1.0f) / intrinsics.fx;
		float y = (depthCoord.y - intrinsics.cy - 1.0f) / intrinsics.fy;

		//%distorsion correction
		float x0 = x;
		float y0 = y;

		for (int j = 0; j < 4; j++) {
			float r2 = pow(x, 2) + pow(y, 2);
			float p = 1 / (1 + intrinsics.k2 * r2 + intrinsics.k4 * pow(r2, 2) + intrinsics.k6 * pow(r2, 3));
			x = x0 * p;
			y = y0 * p;
		}

		// convert to meters
		return cv::Point3f(
								x * (float)depth * 0.001f,
								-y * (float)depth * 0.001f,
								(float)depth * 0.001f
							);
	};

	/**
	 * Returns an empty vector for the cameras.
	 * @return camera vector
	 */
	std::vector<std::string> getCameras() {
		return std::vector<std::string>();
	};

private:
	/** Color Image. */
	cv::Mat color;
	/** Depth Image. */
	cv::Mat depth;
	/** IR-Image. */
	cv::Mat ir;

	CameraIntrinsics intrinsics;
};

#endif // !KINECT_EMU_H